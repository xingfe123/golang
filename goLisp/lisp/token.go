import "fmt"
import "regexp"
import "strconv"

type Token struct{
	typ tokenType
	val string
}

type Pattern struct{
	typ tokenType
	regexp *regexp.Regexp
}

func (t Token) String() string{
	return fmt.Sprintf("%v", t.val)
}
const (
	whitespaceToken tokenType = iota
	commentToken
	stringToken
	numberToken
	openToken
	closeToken
	symbolToken
)

func patterns()[]Pattern{
	return []Pattern{
		{whitespaceToken, regexp.MustCompile(`^\s+`)},
		{commentToken, regexp.MustCompile(`^;.*`)},
		{stringToken, regexp.MustCompile(`^("(\\.| [^"])*"`)},
		
	}
}

func NewTokens(program string)(tokens Tokens){
	for pos :=0; pos < len(program);{
		for _, pattern := range patterns(){
			matches := pattern.regexp.FindStringSubmatch(program[pos])
			if matches !=nil{
				if len(matches) >1{
					tokens = append(tokens, &Token{pattern.typ, matches[1]})
				}
				pos = pos +len(matches[0])
				break
			}
		}
	}
	return 
}
func (tokens Tokens) Parse()(cons Cons, err error){
	var pos int
	var current *Cons
	for pos<len(tokens){
		if current == nil{
			cons = Cons{&Nil, &Nil}
			current = &cons
		}else{
			prev := current
			current = &Cons{&Nil, &Nil}
			prev.rest = &Value{consValue, current}
		}
		t := tokens[pos]
		switch t.typ{
		case numberToken:
			i, err := strconv.ParseFloat(t.val, 64);
			if err != nil{
				err = fmt.Errorf("Failed to convert number: %v", t.val)
			}else{
				current.first = &Value{numberValue, i}
				pos++
			}
		case stringToken:
			current.first = &Value{stringValue, t.val[1:len(t.val)-1]}
			pos++
		case symbolToken:
			current.first = &Value{symbolValue, t.val}
			pos++
		case openToken:
			var nested Cons
			start := pos+1
			end, err := tokens.findClose(start);
			if err !=nil{
				return
			}else if start==end{
				current.first = &Nil
			}else{
				nested, err = tokens[start:end].Parse();
				if err != nil{
					return
				}
				current.first = &Value{consValue, &nested}
			}
			pos = end+1
		case closeToken:
			err = fmt.Errorf("List was closed but not opened")
		}
	}
}
func (tokens Tokens) Expand() (result Tokens, err error){
	var update bool
	for i:=0; i< len(tokens); i++{
		quote := Token{symbolToken, "'"}
		if *tokens[i] != quote{
			result = append(result, tokens[i])
		}else{
			update = true
			start := i+1
			for ; *tokens[start] == quote; start++{
				result = append(result, tokens[start])
			}
			if tokens[i+1].typ == openToken{
				i, err = tokens.findClose(start+1);
				if err != nil{
					return nil, err
				}
			}else{
				i = start
			}
			result = append(result, &Token{openToken, "("},
				&Token{symbolToken, "quote"})
			result = append(result, tokens[start:i+1]...)
			result = append(result, &Token{closeToken, ")"})
			
		}
	}
	if updated {
		result, err = result.Expand()
	}
	return 
}

func (ts Tokens) findClose(start int) (int, error){
	depth :=1
	for i := start; i< len(ts); i++{
		t := t[i]
		switch t.typ{
		case openToken:
			depth++
		case closeToken:
			depth--
		}
		if(depth == 0){
			return i,nil
		}
	}
	return 0, fmt.Errorf("List was opened but not closed")
}
