package main

import (
	"flag"
	"fmt"
	"os"
	"strings"
	"bufio"
)

func main(){
	file := flag.String("file", "", "File to interpret")
	repl := flag.Bool("i", false, "Interactive mode")
	flag.Parse()
	if *repl{
		Repl()
	}else if *file != ""{
		
	}
}
func Repl(){
	fmt.Printf("Welcome to the Lisp REPL\n")
	reader := bufio.NewReader(os.Stdin)
	expr := ""
	for{
		if expr == ""{
			fmt.Printf("\n> ")
		}
		line, _ := reader.ReadString('\n')
		expr = fmt.Sprintf("%v%v", expr, line)
		expr = strings.TrimSpace(expr)
		if CheckBracket(&expr){
			res, err := lisp.EvalString(expr)
			if err!=nil{
				fmt.Printf("ERROR: %v\n", err)
			}else{
				if res == lisp.Nil{
					fmt.Println("Unspecified")
				}else{
					fmt.Printf(";Value: %v", res.Inspect())
				}
			}
		}else{
			fmt.Printf("Error: Malformed expression: %v", line)
			expr = ""
		}
	}
}
func CheckBracket(expr *string) bool{
	i := 0
	j := len(*expr)-1
	for{
		if i<j && (*expr)[i] !='('{
			i++
		}else if i<j && (*expr)[j] !=')'{
			j--
		}else if (*expr)[i]== '(' && (*expr)[j] ==')'{
			i++
			j--
		}else{
			return false 
		}
		
	}
	return true
}

