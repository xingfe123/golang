package main

import (
	"fmt"
	"io"
	"os"
	"syscall"
)

const (
	Pi      float64 = 3.14
	size    int64   = 1024
	eof             = -1
	u, v    float64 = 0, 3
	a, b, c         = 3, 4, "foo"
	mask            = 1 << 3
	c0              = iota
)
const (
	Sunday = iota
	Monday
	Tuesday
	Wednesday
	Thursday
	Friday
	Staturday
	numberOfDays
)

//
func sum(args ...int64) int64 {
	var val int64 = 0
	for _, arg := range args {
		val += arg
	}
	return val
}

//
func stat(name string) (file FileInfo, err error) {
	var stat syscall.Stat_t
	err = syscall.Stat(name, &stat)
	if err != nil {
		return nil, &PathError("stat", name, err)
	}
	return
}
func count(ch chan int) {
	ch <- 1
}
func run() {
	chs := make([]chan int, 10)
	for i := 0; i < 10; i++ {
		chs[i] = make(chan int)
		go count(chs[i])
	}
	for _, ch := range chs {
		fmt.Println(<-ch)
	}
}
func main() {
	var i int64 = 0
	x := func() {
		var j int64 = 0
		fmt.Println("i:%d%d", i, j)
	}
	infileName := "openfile"
	outfileName := "outfile"
	if infile, err := os.Open(infileName); err != nil {
		painc(err)
	}
	defer infile.Close()

	if outfile, err := os.Open(outfileName); err != nil {
		painc(err)
	}
	defer outfile.Close()
	buff := make([]byte, 1024)
	for {
		n, err := infile.Read(buff)
		if err != nil && err != io.EOF {
			painc(err)
		}
		if n == 0 {
			break
		}
		if _, err := outfile.Write(buff[:n]); err != nil {
			painc(err)
		}
		// do something for buff[:n]
	}
}
