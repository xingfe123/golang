package ml

import (
	"fmt"
	"xingfe91/ml/core"
	"xingfe91/ml/math"
	"xingfe91/ml/text"
)

///////////////////////////////////////////
// nan bayes
func createVocabList(file *text.TextFile) (vs *text.Vocab) {
	vs = text.MakeEmptyVocab()
	for i := 0; i < file.Size(); i++ {
		vs.Add(file.GetWord(i))
	}
	return vs
}
func createWordVec(file *text.TextFile, vs *text.Vocab) *text.Vec {
	vec := text.MakeEmptyVec()
	for i := 0; i < file.Size(); i++ {
		word := file.GetWord(i)
		if vs.Contain(word) {
			vec.Add(word)
		} else {
			fmt.Print("the word: %s is not in my vocabulary", word.Text())
		}
	}
	return vec
}
func createBayesClassifer(ds *core.LabelSet) {
	prob := make(map[core.Label]*text.Vec)
	labels := ds.GetLabels()
	for i := 0; i < labels.Size(); i++ {
		prob[labels.Get(i)] = text.MakeEmptyVec()
	}
	for i := 0; i < ds.Size(); i++ {
		dat := ds.Get(i)
		prob[dat.GetLabel()].AddAllWords(dat)
	}
	return core.MakeBayesClassifer(prob)
}

////////////////////////////////////////////////////////////////

func entropy(ds *core.LabelSet) float64 {
	labelsCount := map[core.Label]int{}
	for i := 0; i < ds.Size(); i++ {
		labelsCount[ds.GetLabel(i)]++
	}
	ent := 0.0
	for _, number := range labelsCount {
		prob := float64(number) / float64(ds.Size())
		ent -= prob * math.Ln(prob)
	}
	return ent
}

///////////////////////////////////////////////////////////////////

func splitedDataSet(ds *core.LabelSet, f int) []*core.LabelSet {
	return nil ///////////////////////////
}
func chooseBestFeature(ds *core.LabelSet) int {
	numFeature := ds.GetDataDim()
	baseEntropy := entropy(ds)
	bestInfoGain := 0.0
	bestFeature := -1
	for i := 0; i < numFeature; i++ {
		newEntropy := 0.0
		splitedSet := splitedDataSet(ds, i)
		for _, subSet := range splitedSet {
			newEntropy +=
				float64(subSet.Size()) / float64(ds.Size()) * entropy(subSet)
		}
		infoGain := baseEntropy - newEntropy
		if infoGain > bestInfoGain {
			bestFeature = i
			bestInfoGain = infoGain
		}
	}
	return bestFeature
}
func majorityCnt(ds *core.LabelSet) (res core.Label) {
	labels := map[core.Label]int{}
	for i := 0; i < ds.Size(); i++ {
		labels[ds.GetLabel(i)]++
	}
	max := -1
	for label, number := range labels {
		if number > max {
			max = number
			res = label
		}
	}
	return res
}

func createTree(ds *core.LabelSet) {

}

//////////////////////////////////////////////////////////////////
func KnnClassify(X *core.Data,
	ds *core.LabelSet, k int) (result core.Label) {

	dataSize := ds.Size()
	distances := make([]float64, dataSize)
	for i := 0; i < dataSize; i++ {
		distances[i] = core.NormDistance(ds.Get(i), X)
	}
	datas := make([]int, k)
	for i := 0; i < dataSize; i++ {
		datas[i] = i
	}
	for i := k - 1; i > 0; i-- {
		if distances[datas[i]] < distances[datas[(i-1)/2]] {
			datas[i], datas[(i-1)/2] = datas[(i-1)/2], datas[i]
		}
	}
	for i := k; i < dataSize; i++ {
		if distances[i] <= distances[datas[0]] {
			continue
		}
		datas[0] = i

		hole := 0
		for {
			child := hole*2 + 1
			if child+1 <= k-1 &&
				distances[datas[child+1]] > distances[datas[child]] {
				child++
			}
			if child <= k-1 &&
				distances[datas[child]] > distances[datas[hole]] {
				datas[hole], datas[child] = datas[hole], datas[child]
				hole = child
				continue
			}
			break
		}
	}
	dict := map[core.Label]int{}
	for i := 0; i < k; i++ {
		dict[ds.GetLabel(datas[i])]++
	}
	max := -1
	for d, n := range dict {
		if max < n {
			result = d
			max = n
		}
	}
	return
}
