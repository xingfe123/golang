package core

func getRecordSet() *RecordSet {
	return nil //todo
}
func makeSimpleRecord(i int) IntSet {
	return *makeIntSet() // todo
}

//////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
func createC1(rs *RecordSet) *RecordSet {
	c1 := getRecordSet()
	for i := 0; i < rs.Size(); i++ {
		record := rs.Get(i)
		for j := 0; j < record.Size(); j++ {
			c1.Add(makeSimpleRecord(record.Get(j)))
		}
	}
	return c1
}

//////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
////////////////////////////////////
type RecordSet struct {
	values []IntSet
}

func (rs RecordSet) Size() int {
	return len(rs.values)
}
func (rs RecordSet) Get(i int) IntSet {
	return rs.values[i]
}
func (rs RecordSet) Add(r IntSet) {
	rs.values = append(rs.values, r)
}

//////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////
type IntSet struct { //todo
	values []int
}

func makeIntSet() *IntSet {
	return &IntSet{values: make([]int, 0)}
}
func (is IntSet) Get(i int) int {
	return is.values[i]
}
func (is IntSet) Size() int {
	return len(is.values)
}
func (is IntSet) Add(k int) {
	is.values = append(is.values, k)
	size := len(is.values)
	if size <= 1 {
		return
	}
	i := size - 1
	for ; i > 0; i-- {
		if is.values[i-1] > is.values[i] {
			is.values[i] = is.values[i-1]
		}
	}
	is.values[i] = k
}
func (is IntSet) Issub(other IntSet) bool {
	i := 0
	for j := 0; j < other.Size(); j++ {
		if is.values[i] == other.values[j] {
			j++
		} else if is.values[i] < other.values[j] {
			return false
		}
	}
	return i == is.Size()
}

////////////////////////////////////////////////
type DictInt struct {
	values []int
	sets   []IntSet
	size   int
}

func (di DictInt) set(i, j int) {
	di.values[i] = j
}

func (di DictInt) getKey(i int) IntSet {
	return di.sets[i]
}
func (di DictInt) getValue(i int) int {
	return di.values[i]
}
func (di DictInt) Size() int {
	return di.size
}

////////////////////////////////////////
type DictFloat struct {
	values []float64
}

func (df DictFloat) set(i int, f float64) {
	df.values[i] = f
}

////////////////////////////////////////////////////////////////////
func scanD(rs *RecordSet, ck *RecordSet,
	minSupport float64) DictFloat {
	var ssCnt DictInt
	for i := 0; i < rs.Size(); i++ {
		record := rs.Get(i)
		for j := 0; j < ck.Size(); j++ {
			can := ck.Get(i)
			if can.Issub(record) {
				ssCnt.set(i, ssCnt.getValue(i)+1)
			}
		}
	}
	nums := float64(rs.Size())
	var ret DictFloat
	for i := 0; i < ssCnt.Size(); i++ {
		value := ssCnt.getValue(i)
		support := float64(value) / nums
		if support >= minSupport {
			ret.set(i, support)
		}

	}
	return ret
}

////////

////////////////////////////////////////
