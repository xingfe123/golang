package core

import (
	"math"
)

func Ln(v Value) DoubleValue {
	x, ok := v.(DoubleValue)
	if !ok {
		panic("hello")
	}
	return makeDoubleValue(math.Log(x.d))
}

type Value interface {
	AddTo(Value)
	Sub(Value) Value
	Mult(Value) Value
	Div(Value) Value
	GreaterThan(Value) bool
}

/////////////////////////

///////////////
type IntValue struct {
	d int
}

func (self IntValue) Sub(v Value) Value {
	other, ok := v.(IntValue)
	if ok {
		return makeIntValue(self.d - other.d)
	}
	panic("can't covert")
}
func (self IntValue) AddTo(v Value) {
	other, ok := v.(IntValue)
	if ok {
		self.d += other.d
	}
	panic("can't covert")
}
func (self IntValue) Mult(v Value) Value {
	other, ok := v.(IntValue)
	if ok {
		return makeIntValue(self.d + other.d)
	}
	panic("can't covert")
}
func makeIntValue(i int) IntValue {
	return IntValue{d: i}
}
func (self IntValue) Div(v Value) Value {
	other, ok := v.(IntValue)
	if !ok {
		panic("int")
	}
	return makeIntValue(self.d / other.d)
}
func (self IntValue) GreaterThan(v Value) bool {
	other, ok := v.(IntValue)
	if !ok {
		panic("int")
	}
	return self.d > other.d
}

/////////////
type DoubleValue struct {
	d float64
}

//////////////////////////
func makeDoubleValue(v float64) DoubleValue {
	return DoubleValue{d: v}
}

func (self DoubleValue) Mult(other Value) Value {
	return makeDoubleValue(1.0)
}
func (self DoubleValue) GreaterThan(_other Value) bool {
	other, ok := _other.(DoubleValue)
	if !ok {
		return false
	}
	return self.d > other.d
}
func (self DoubleValue) AddTo(_other Value) {
	other, ok := _other.(DoubleValue)
	if !ok {
		panic("can't covert to DoubleValue")
	}
	self.d += other.d
}

func (self DoubleValue) Div(_other Value) Value {
	other, ok := _other.(DoubleValue)
	if !ok {
		panic("can't covert to DoubleValue")
	}
	return makeDoubleValue(self.d / other.d)
}
func (self DoubleValue) Ln() Value {
	return makeDoubleValue(math.Log(self.d))
}

/////////////////////////////////////////////////////////////
