package core

import (
	"math"
)

////////////
func (self DoubleValue) Sub(v Value) Value {
	other, ok := v.(DoubleValue)
	if !ok {
		panic("")
	}
	return makeDoubleValue(self.d - other.d)

}

//////////
func sigmoid(v Value) float64 {
	inx := v.(DoubleValue).d
	return 1.0 / math.Exp(-inx)
}

type DoubleVector struct {
	d []float64
}

func makeDoubleVectorOnes(n int) Vector {
	return DoubleVector{d: make([]float64, n, 1.0)}
}
func (self DoubleVector) Size() int {
	return len(self.d)
}
func (self DoubleVector) GetLabel(i int) Label {
	return makeIntValue(i)
}

func (self DoubleVector) Mult(v Vector) Value {
	other, ok := v.(DoubleVector)
	if !ok {
		return makeDoubleValue(0.0)
	}
	s := 0.0
	for i := 0; i < v.Size(); i++ {
		s += self.d[i] * other.d[i]
	}
	return makeDoubleValue(s)
}
func (self DoubleVector) AddTo(vec Vector) {
	other, ok := vec.(DoubleVector)
	if ok {
		for i := 0; i < self.Size(); i++ {
			self.d[i] += other.d[i]
		}
	}
	panic("hello")
}
func (self DoubleVector) Set(l Label, v Value) {
	i, ok1 := l.(IntValue)
	d, ok2 := v.(DoubleValue)
	if !ok1 || !ok2 {
		panic("Hello")
	}
	self.d[i.d] = d.d
}
func (self DoubleVector) GetValue(i int) Value {
	return makeDoubleValue(self.d[i])
}
func (self DoubleVector) Get(l Label) Value {
	i, ok := l.(int)
	if ok {
		return self.GetValue(i)
	}
	panic("hello")
}

////////////////
//temp
///
func (self RegissionSet) GetData(i int) Vector {
	return makeDoubleVectorOnes(1) //todo
}
func (self RegissionSet) GetValue(i int) Value {
	return makeDoubleValue(0.0) //todo
}
func (self DoubleVector) Smult(v Value) Vector {
	return self
}
func GradAscent(ds RegissionSet) Vector {
	n := 1
	weights := makeDoubleVectorOnes(n)
	m := ds.Size()
	alpha := makeDoubleValue(0.01)
	for i := 0; i < m; i++ {
		h := sigmoid(weights.Mult(ds.GetData(i)))
		error := ds.GetValue(i).Sub(makeDoubleValue(h))
		weights.AddTo(ds.GetData(i).Smult(alpha).Smult(error))
	}
	return weights
}
