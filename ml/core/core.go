package core

////////////////

/////////////////
type Classifer interface {
}
type Bayes struct {
	prob map[Label]Vector
}

func (self *Bayes) GetAllDataLabel() Labels {
	//todo
	return makeIntLables(1)
}

func (self *Bayes) classify(d Vector) (l Label) {
	max := makeDoubleValue(-1.0)
	v := makeProbilityVector(d, self.GetAllDataLabel())
	for label, vec := range self.prob {
		sum := vec.Mult(v)
		for i := 0; i < v.Size(); i++ {
			word := v.GetLabel(i)
			value := v.GetValue(i)
			sum.AddTo(value.Mult(Ln(vec.Get(word))))
		}
		if sum.GreaterThan(max) {
			l = label
		}
	}
	return
}

type KNN struct {
}

//////////////////////////////
func NormDistance(X, Y *Vector) float64 {
	//todo
	return 0.0
}
