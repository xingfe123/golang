package core

func makeProbilityVector(d Vector, ls Labels) Vector {
	return WordVector{}
}

type Vector interface {
	Size() int
	AddTo(Vector)
	Mult(Vector) Value
	Smult(Value) Vector
	Set(Label, Value)
	Get(Label) Value
	GetLabel(int) Label
	GetValue(int) Value
}
type WordVector struct {
	vec map[Label]int
}

func (self WordVector) AddTo(v Vector) {
	words, ok := v.(WordVector)
	if !ok {
		panic("Hello")
	}
	for l, v := range words.vec {
		self.vec[l] += v
	}
}
func (self WordVector) Smult(v Value) Vector {
	other, ok := v.(IntValue)
	if !ok {
		panic("ok")
	}
	for l, _ := range self.vec {
		self.vec[l] *= other.d
	}
	return self
}
func (self WordVector) Get(l Label) Value {
	return makeIntValue(self.vec[l])
}
func (self WordVector) Size() int {
	return -1 //inf
}

///////////////
func (self WordVector) Set(l Label, v Value) {
	// todo
}
func (self WordVector) GetLabel(i int) Label {
	//todo
	return IntValue{0}
}
func (self WordVector) GetValue(i int) Value {
	//todo
	return makeDoubleValue(1.0)
}
func (self WordVector) Mult(other Vector) Value {
	return makeDoubleValue(1.0)
}

func makeWordVector(d Vector, labels Labels) (l Vector) {
	return WordVector{}
}

func MakeEmptyWordVector() Vector {
	//todo
	return WordVector{}
}
