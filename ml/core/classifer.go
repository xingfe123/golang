package core

func MakeBayesClassifer(prob map[Label]Vector) *Bayes {
	total := MakeEmptyWordVector()
	for _, vec := range prob {
		total.AddTo(vec)
	}
	ps := make(map[Label]Vector)
	for label, vec := range prob {
		for i := 0; i < vec.Size(); i++ {
			vec.Set(vec.GetLabel(i), //makeValue(1.0))
				vec.GetValue(i).Div(total.GetValue(i)))
		}
		ps[label] = vec
	}
	return &Bayes{prob: ps}
}
