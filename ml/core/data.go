package core

///////////////////////

/////////////////

//////////////////////

type Label interface {
}
type DataSet interface {
	Get(i int) Vector
	Set(i int, v Value)
}

/////////////////////////////////////////
//////////////////////////////////////////////////////
type LabelSet struct {
	size    int
	Lables  []Label
	dataSet DataSet
}

func (self *LabelSet) Size() int {
	return self.size
}

////////////////////////////////////////////////////////

func (ds *LabelSet) GetLabel(i int) Label {
	return ds.Lables[i]
}
func (ds *LabelSet) Get(i int) Vector {
	return ds.dataSet.Get(i)
}
func (ds *LabelSet) GetDataDim() int {
	return 1 // todo
}

type RegissionSet struct {
	s int
}

func (self RegissionSet) Size() int {
	return self.s
}

////////////////////////
type Labels interface {
	Size() int
	Get(int) Label
}
type IntLabels struct {
	i int
}

func makeIntLables(n int) Labels {
	return IntLabels{i: n}
}
func (self IntLabels) Size() int {
	return self.i
}

/////////////

//////////////

func (self IntLabels) Get(i int) Label {
	return makeIntValue(i)
}
