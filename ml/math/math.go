package math

import (
	"flag"
	"fmt"
	"io"
	"os"
)

type Probability struct {
}

func (p *Probability) entropy() float64 {
	return 1.0
}
func IsEqual(f1, f2 float64) bool {
	return f1-f2 < 0.1
}

type PersonInfo struct {
	id      string
	name    string
	address string
}

func myfunc(args ...int) {
	myfunc(args[1:]...)
	for _, arg := range args {
		fmt.Println(arg)
	}
}
