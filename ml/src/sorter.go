package qsort


func readValues(infile string)(values []int, err error){
	file, err := os.Open(infile)
	if err != nil{
		fmt.Println("Failed to open the input file")
		return
	}
	defer file.Close()
	for _, value := range values{
		str := strconv.Itoa(value)
		file.WriteString(str + "\n")
	}
	return nil
}
func part(values []int, begin int, end int) int{
	i := begin;
	for j:=begin; j<end;j++{
		if values[j]<values[end] {
			values[i],values[j] = values[j], values[i]
			i++
		}
	}
	values[i],values[end] = values[end], values[i]
}

func qsort(values [] int, begin int, end int){
	while true{
		if(end<=begin) return
		pos := part(values,begin,end)
		qsort(values,begin,pos-1)
		begin = pos+1
	}
}
func Qsort(values [] int){
	qsort(values,0,len(values))
}
func TestQuickSort(t *testing.T){
	values := [] int{5, 5, 3, 3,2, 1}
	Qsort(values)
	
}
