package simplemath

import "math"
import "testing"
import "fmt"

func IsEqual(value, other , p float64) bool{
	return math.Fdim(value, other) < p
}

func test(x int){
	str := "hello, world"
	n := len(str)
	for i:=0; i< n;i++{
		fmt.Println(i, str[i])
	}
	var v int
	var v2 string
	var v3 [10] int
	var v4 [] int
	var v5 struct {
		f int
	}
	var v6 *int
	var v7 map[string]int
	var v8 func(a int) int
	var v9 int = 10
	var v0 = 10
	v10 :=10
}

func TestAdd(t* testing.T){
	r := Add(1,2)
	if r != 2{
		t.Error("Add(1,2) failed. ")
	}
}

func Add(a int, b int) int {
	return a+b
}


func Sqrt(i int) int{
	v := math.Sqrt(float64(i))
	return int(v)
}
