package matrix



type Matrix struct{
	rows, cols int
	data []int
}
type Vector struct{
	data []int
}
func (vc *Vector) size() int{
	return len(vc.data)
}
func (vc *Vector) Get(i int) int{
	return vc.data[i]
}
func (m *Matrix)Product(vc * Vector) (res *Vector){
	x := make([]int, vc.size())
	for i :=0; i< vc.size(); i++{
		for k :=0; k< m.cols; k++{
			x[i] += m.Get(i,k) * vc.Get(i)
		}
	}
	res.data = x
	return
}
func (m *Matrix)index(r, c int) int{
	return (r-1)* m.cols + (c-1)
}
func (m *Matrix) Set(r, c, val int){
	m.data[m.index(r,c)] = val
}
func (m *Matrix) Get(r, c int) int{
	return m.data[m.index(r,c)]
}
func Zero(r, c int) Matrix{
	return Matrix{
		rows : r,
		cols : c,
		data : make([]int, r*c),
	}
}
func One(n int) Matrix{
	A := Zero(n, n)
	for i :=0 ; i< n; i++{
		A.Set(i, i, 1)
	}
	return A
}


func Fib(n int) Matrix{
	A := Zero(2, 2)
	for i :=0 ; i< 2; i++{
		for j :=0 ; j<2; j++{
			if i==0 || j==0 {
				A.Set(i, i, 1)
			}
		}
	}
	return Power(A, n)
}


func Power(A Matrix ,n int) Matrix{
	if(n==1) return A
	half := Power(A, n/2)
	result := Power(half, 2)
	if(n&01==1) result = result.mul(A)
	return result
}

func (Matrix* A) mul(Matrix* B){
	C := Zero(A.rows, B.cols)
	for i:=0; i< A.rows; i++{
		for j:=0; j< B.cols; j++{
			for k:=0; k< A.cols; k++{
				C.Set(i, j, C.Set(i, j)+ A.Get(i,k)*B.Get(k,j))
			}
		}
	}
}


func Add(A, B *Matrix) (N *Matrix ){
	*N = Matrix{
		rows : A.rows,
		cols : A.rows,
		data : A.data,
	}
	for i:=0; i< len(N.data);i++{
		N.data[i] += B.data[i]
	}
	return 
}

