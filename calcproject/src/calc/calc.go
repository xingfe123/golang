package calc

import "os"
import "fmt"
import "simplemath"
import "strconv"

var Usage = func(){
    fmt.Println("Usage: calc commmand [argumnets] ...")
    fmt.Println("\nThe commands are:\n")
}


func main(){
     args := os.Args
     if args == nil || len(args) <2{
        Usage()
        return
     }
     switch args[0]{
	 case "add":
		 if len(args) !=3{
			 fmt.Println("")
			 return
		 }
		 v1, err1 := strconv.Atoi(args[1])
		 v2, err2 := strconv.Atoi(args[2])
		 if err1 != nil || err2 != nil{
			 fmt.Println("")
			 return
		 }
		 ret := v1 + v2
	 case "sqrt":
		 if len(args) !=2{
			 fmt.Println("Usage: calc sqrt <integer>")
			 return
		 }
		 v, err := strconv.Atoi(args[1])
		 if err != nil{
			 fmt.Println("Usage: calc sqrt <integer")
			 return
		 }
		 ret := simplemath.Sqrt(v)
		 fmt.Println("Result: ", ret)
	 default:
		 Usage()
     }    
}
