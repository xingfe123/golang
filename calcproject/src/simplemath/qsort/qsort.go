package qsort

import "os"
import "fmt"
import "strconv"
import "testing"

type Integer int
type Rect struct{
	x, y float64
	width, heigh float64
}

func (r *Rect) Area() float64{
	return r.width * r.heigh
}
func Integer_Less(a int, b int) bool{
	return a<b
}



func readValues(infile string)(values []int, err error){
	file, err := os.Open(infile)
	if err != nil{
		fmt.Println("Failed to open the input file")
		return
	}
	defer file.Close()
	for _, value := range values{
		str := strconv.Itoa(value)
		file.WriteString(str + "\n")
	}
	return 
}
func part(values []int, begin int, end int) (i int ){
	i = begin;
	for j:=begin; j<end; j++{
		if values[j] < values[end]{
			values[i], values[j] = values[j], values[i]
			i++
		}
	}
	values[i],values[end] = values[end], values[i]
	return 
}

func qsort(values []int, begin int, end int)(){
	for{
		if end<=begin {
			return
		}
		pos := part(values,begin,end)
		qsort(values,begin,pos-1)
		begin = pos+1
	}
}
func Qsort(values [] int){
	qsort(values,0,len(values))
}
func TestQuickSort(t *testing.T){
	values := [] int{5, 5, 3, 3,2, 1}
	Qsort(values)
}
func isort(values []int, size int){
	if size <= 1{
		return 
	}
	for i := 1; i<size; i++{
		hole := i
		key := values[hole]
		for ; hole>=1 && values[hole-1] > key; hole--{
			values[hole] = values[hole-1]
		}
		values[hole] = key
	}
}
func ssort(values []int, size int){
	for j :=size ; j> 0 ;j--{
		max := 0
		for i := 1; i < j; i++{
			if values[max] <= values[i]{
				max = i;
			}
		}
		values[max], values[j-1] = values[j-1], values[max]
	}
}
func msort(values []int, begin int, last int){
	if last-begin <= 1 {
		return
	} 
	half := (last-begin)/2
	left := make([]int, half)
	for i :=0; i<half; i++{
		left[i] = left[i+begin]
	}
	msort(values, half, last)
	msort(left, begin, half)
	i := 0
	k := half
	j := 0
	for ;i < half; j++{
		if left[i] < values[k] || k==last{
			values[j] = left[i]
			i++
		}else{
			values[j] = values[k]
			k++
		}
	}
}
func hsort(values []int, size int){
	for i := size-1; i>0; i--{
		p := (i-1)/2
		if values[i] > values[p]{
			values[p], values[i] = values[i], values[p]
		}
	}
	for last := size-1; last>0; last--{
		values[last], values[0] = values[0], values[last]
		hole := 0
		for{
			left := 2*hole+1
			right := 2*hole+2
			child := hole
			if left < last && values[left] < values[child]{
				child = left
			}
			if right < last && values[right] < values[child]{
				child = right
			}
			if child == hole {
				return
			}
			values[child], values[hole] = values[hole], values[child]
		}
	}
}

func xsort(values []int, size int){
	for i := size-1 ; i>0; i--{
		isort :=true
		for j :=0 ; j<i; j++{
			if values[j] > values[j+1]{
				values[j], values[j+1] = values[j+1], values[j]
				isort = false;
			}
		}
		if isort{
			return 
		}
	}
}

