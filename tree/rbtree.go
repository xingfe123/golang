package tree

import (
	"errors"
)

type Color bool

const (
	BLACK, RED Color = true, false
)

var (
	ErrorKeyIsNil   = errors.New("key is nil")
	ErrorNotFindKey = errors.New("no find key")
)

type RBNode struct {
	left, right, p *RBNode
	color          Color
	key            int
}

func grandparent(x *RBNode) *RBNode {
	if x == nil {
		return nil
	} else if y := x.p; y == nil {
		return nil
	} else {
		return y.p
	}
}
func uncle(x *RBNode) *RBNode {
	if g := grandparent(x); g == nil {
		return nil
	} else if x.p == g.left {
		return g.right
	} else {
		return g.left
	}
}
func sibling(x *RBNode) *RBNode {
	if x == x.p.left {
		return x.p.right
	}
	return x.p.left
}
func color(x *RBNode) Color {
	if x == nil {
		return BLACK
	}
	return x.color
}

type RBTree struct {
	root *RBNode
	size int
}

func (t RBTree) up_rotate(x *RBNode) {
	y := x.p
	if y == nil {
		return
	} else {
		if y.right == x {
			x.left = y
			y.right = x.left
		} else {
			x.right = y
			y.left = x.right
		}
		y.p = x
		if y.p == nil {
			t.root = x
		} else if y == y.p.left {
			y.p.left = x
		} else {
			y.p.right = x
		}
	}
}
func NewRBTree() *RBTree {
	return &RBTree{}
}
func (t RBTree) delete(z *RBNode) {
	if z.p == nil {
		t.root = nil
		return
	}
	if z.p.left == z {
		z.p.left = nil
	} else if z.p.right == z {
		z.p.right = nil
	}
	if l := z.left; l != nil {
		t.insert(l)
	}
	if r := z.right; r != nil {
		t.insert(r)
	}

}
func (t RBTree) insert(z *RBNode) {
	t._insert(t.root, z)
}
func (t RBTree) _insert(x, z *RBNode) {
	if z == nil {
		return
	}
	var y *RBNode
	for x != nil {
		y = x
		if z.key < x.key {
			x = x.left
		} else {
			x = x.right
		}
	}
	z.p = y
	if y == nil {
		t.root = z
	} else if z.key < y.key {
		y.left = z
	} else {
		y.right = z
	}
	z.left, z.right, z.color = nil, nil, RED
	t.rb_insert_fixup(z)
}

//asert x != nil;
func (t RBTree) insert_fixup(x *RBNode) {
	if x.p == nil {
		x.color = BLACK
		t.root = x
		return
	} else if y := x.p; y.color == BLACK {
		return

	} else if u := uncle(x); color(u) == RED { // color(u) 蕴含 y.p !=nil
		y.color, u.color, y.p.color = BLACK, BLACK, RED
		t.insert_fixup(y.p)
	} else if g := grandparent(x); g == nil {
		y.color = BLACK
		t.root = y
		return
	} else if (g.left == y && y.left == x) ||
		(g.right == y && y.right == x) {
		t.up_rotate(y)
		y.color, y.left.color, y.right.color = BLACK, RED, RED
		return
	} else {
		t.up_rotate(x)
		t.up_rotate(x)
		x.color, x.left.color, x.right.color = BLACK, RED, RED
		return
	}

}
func (t RBTree) rb_insert_fixup(n *RBNode) {
	if n == nil {
		return
	} else if n.p == nil {
		t.root = n
		n.color = BLACK
	} else if n.p.color == BLACK {
		return
	} else if uncle(n) != nil && uncle(n).color == RED {
		n.p.color = BLACK
		uncle(n).color = BLACK
		grandparent(n).color = RED
		t.rb_insert_fixup(grandparent(n))
	} else if n == n.p.right && n.p == grandparent(n).left {
		t.left_rotate(n.p)
		n = n.left
	} else if n == n.p.left && n.p == grandparent(n).right {
		t.right_rotate(n.p)
		n = n.right
	} else {
		n.p.color = BLACK
		grandparent(n).color = RED
		if n == n.p.left && n.p == grandparent(n).left {
			t.right_rotate(grandparent(n))
		} else {
			t.left_rotate(grandparent(n))
		}
	}
}

func (t RBTree) left_rotate(n *RBNode) {

}
func (t RBTree) right_rotate(n *RBNode) {

}
