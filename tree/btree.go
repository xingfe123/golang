package tree

import "time"

type StreamBTree struct {
	BtreeMetadata
	opChan  chan *treeOperation
	duplist map[int64]int
}
type treeOperation struct {
	errChan   chan *error
	TreeLog   int
	Key       int
	valueChan chan int
}

func (st StreamBTree) Marshal(s string) {
	//todo
}

type BtreeMetadata struct {
}
type Oper struct {
	errChan   chan *error
	valueChan chan int
	Key       int
	TreeLog   int
}

func (op Oper) GetAction() string {
	return "ok" //todo
}
func NewStreamBtree() *StreamBTree {
	tree := &StreamBTree{
		duplist: make(map[int64]int),
		opChan:  make(chan *treeOperation),
	}
	go tree.run()
	return tree
}
func (t StreamBTree) insert(int) *error {
	return nil
}
func (t StreamBTree) delete(int) *error {
	return nil
}
func (t StreamBTree) search(int) (int, *error) {
	return 0, nil
}
func (t StreamBTree) gc() {
	// todo
}
func (op treeOperation) GetAction() string {
	return "42"
}
func (t StreamBTree) run() {
	tick := time.Tick(time.Second * 2)
	for {
		select {
		case op := <-t.opChan:
			switch op.GetAction() {
			case "insert":
				op.errChan <- t.insert(op.TreeLog)
			case "delete":
				op.errChan <- t.delete(op.Key)
			case "search":
				rst, err := t.search(op.Key)
				op.valueChan <- rst
				op.errChan <- err
			}
		case <-tick:
			t.gc()
		}
	}
	t.Marshal("treedump.tmp")
}

////////////////////////////////////////////////////////
