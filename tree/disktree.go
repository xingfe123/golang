package tree

const (
	t = 8
)

type BTree struct {
	root *BTreeNode
}
type BTreeNode struct {
	n     int
	key   []int
	child []*BTreeNode
	leaf  bool
}

func makeBTreeNode() *BTreeNode {
	size := 2*t - 1
	return &BTreeNode{
		key:   make([]int, size),
		child: make([]*BTreeNode, size+1),
		n:     size,
	}
}

func BTreeSearch(x *BTreeNode, k int) (*BTreeNode, int) {
	if x == nil {
		return nil, 0
	}
	for {
		var i int
		for start, size := 0, x.n; ; {
			if size == 0 {
				i = start
				break
			} else if i = start + size/2; k < x.key[i] {
				start = i + 1
				size -= size/2 + 1
			} else {
				break
			}
		}
		if x.leaf && i == x.n {
			return nil, 0
		}
		x = x.child[i]
	}
}

func BTreeSplitChild(x *BTreeNode, i int) {
	y := x.child[i] //assert y != nil
	/*

		if (*y).Leaf() {
			z := makeLeafNode(t - 1)
			for j := t; j < 2*t-1; j++ {
				z.setkey(j-t, y.key(j))
			}
		} else {
			z := makeLeafNode(t - 1)
			for j := t; j < 2*t-1; j++ {
				z.setkey(j-t, y.key(j))
			}
			for j := t; j < 2*t; j++ {
				z.setchild(j-t, y.child(j))
			}
		}
		for j := x.size(); j >= i+1; j-- {
			x.child[]j+1, x.child(j))
		}
		x.child[i+1] = &z
	*/
	for j := x.n - 1; j >= i; j-- {
		x.key[j+1] = x.key[j]
	}
	x.key[i] = y.key[t-1]

	x.n++

	/*
		z := newBNode((*y).Leaf(), t-1)
		for j := t; j < 2*t-1; j++ {
			z.key[j-t] = y.key[j]
		}
		if !(*y).Leaf() {
			for j := t; j < 2*t; j++ {
				z.child[j-t] = y.child[j]
			}
		}
		(*y).size() = t - 1
		for j := x.n; j >= i+1; j-- {
			x.child[j+1] = x.child[j]
		}
	*/
}

func BTreeInsert(x *BTreeNode, k int) {
	if x.leaf {
		i := x.n - 1
		for ; i >= 0 && k < x.key[i]; i-- {
			x.key[i+1] = x.key[i]
		}
		x.key[i+1] = k
		x.n++
	} else {
		i := nodeFindJustLess(x, k)
		if x.child[i+1].n == 2*t-1 {
			BTreeSplitChild(x, i+1)
			if x.key[i+1] < k {
				i++
			}
		}
		BTreeInsert(x.child[i], k)
	}
}
func (T BTree) BTreeInsert(k int) {
	r := T.root //assert root != nil
	if r.n == 2*t-1 {
		s := makeBTreeNode()
		T.root = s
		//s.leaf = false
		s.n = 0
		s.child[0] = r
		BTreeSplitChild(s, 0)
		BTreeInsert(s, k)
	} else {
		BTreeInsert(r, k)
	}
}
func nodeFindJustLess(x *BTreeNode, k int) int {
	i := x.n - 1
	for ; i >= 0 && k < x.key[i]; i-- {
	}
	return i
}

func BTreeDelete(x *BTreeNode, i int) {
	if i == x.n-1 {
		BTreeDeleteLast(x)
	} else if i == 0 {
		BTreeDeleteFirst(x)
	} else if left, right := x.child[i], x.child[i+1]; left.n != t-1 {
		x.key[i] = left.key[left.n-1]
		BTreeDeleteLast(left)
	} else if right.n != t-1 {
		x.key[i] = right.key[0]
		BTreeDeleteFirst(right)
	} else {
		left.key[t-1] = x.key[i]
		for j := 0; j < t-1; j++ {
			left.key[t+j] = right.key[j]
		}
		for j := 0; j < t; j++ {
			left.child[t+j] = right.child[j]
		}
		for j := i + 1; j < x.n; j++ {
			x.key[j-1] = x.key[j]
			x.child[j] = x.child[j+1]
		}
		x.n--
		BTreeDelete(left, t-1)
	}
}
func BTreeDeleteLast(x *BTreeNode) {
	for {
		if x.leaf == true {
			x.n--
			return
		}
		lastchild := x.child[x.n]
		x.key[x.n-1] = lastchild.key[lastchild.n-1]
		x = lastchild
	}
}

func BTreeDeleteFirst(x *BTreeNode) {
	for {
		if x.leaf == true {
			for i := 1; i < x.n; i++ {
				x.key[i-1] = x.key[i]
			}
			x.n--
		}
		firstchild := x.child[0]
		x.key[0] = firstchild.key[0]
		x = firstchild
	}
}

func (T BTree) Delete(k int) {
	if x, i := BTreeSearch(T.root, k); x == nil {
		return
	} else {
		BTreeDelete(x, i)
	}
}
