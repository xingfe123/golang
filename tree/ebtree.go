package tree

import "math"

type Ve interface {
	minimum() (int, bool)
	member(x int) bool
	successor(x int) (int, bool)
	insert(x int)
	delete(x int)
	empty() bool
}

//////////////////////////////////////////////////////////
type proVe struct {
	n       int
	u       uint
	summary *Ve
	cluster []*Ve
}
type baseProVe struct {
	n    int
	u    uint
	bits []bool
}

func makebaseVe(max uint) baseProVe {
	return baseProVe{
		n:    0,
		u:    2,
		bits: make([]bool, 2),
	}
}
func up(max uint) uint {
	return max
}

func NewVe(max uint) *Ve {
	var l Ve
	if max <= 1 {
		l = makebaseVe(max)
	} else {
		l = makeProVe(max)
	}
	return &l
}
func makeProVe(max uint) proVe {
	sq := uint(math.Sqrt(float64(max)))
	l := proVe{
		n:       0,
		u:       max,
		summary: NewVe(sq),
		cluster: make([]*Ve, sq),
	}
	for i := uint(0); i < sq; i++ {
		l.cluster[i] = NewVe(sq)
	}
	return l
}
func (ve proVe) high(x int) int {
	return x >> ve.sqrtu()
}
func (ve proVe) low(x int) int {
	return x & (^(1 << ve.sqrtu()))
}
func (ve proVe) sqrtu() uint {
	return uint(math.Sqrt(float64(ve.u)))
}
func (ve proVe) index(x, y int) int {
	return x<<ve.sqrtu() | y
}

/////////////////////////////////////////
func (ve baseProVe) member(x int) bool {
	if x <= 1 {
		return ve.bits[x]
	}
	return false
}
func (ve proVe) member(x int) bool {
	return (*ve.cluster[ve.high(x)]).member(ve.low(x))
}

func (ve baseProVe) minimum() (int, bool) {
	if ve.bits[0] == true {
		return 0, true
	} else {
		return 1, true
	}
	return 0, false
}

func (ve proVe) minimum() (int, bool) {
	if min_cluster, ok := (*ve.summary).minimum(); ok {
		offset, _ := (*ve.cluster[min_cluster]).minimum()
		return ve.index(min_cluster, offset), true
	} else {
		return 0, false
	}
}

func (ve baseProVe) successor(x int) (int, bool) {
	if x == 0 && ve.bits[1] == true {
		return 1, true
	} else {
		return 1, false
	}
}
func (ve proVe) successor(x int) (int, bool) {
	if offset, ok :=
		(*ve.cluster[ve.high(x)]).successor(ve.low(x)); ok == true {
		return ve.index(ve.high(x), offset), true
	} else if succ_cluster, ok1 :=
		(*ve.summary).successor(ve.high(x)); ok1 == true {
		offset, ok = (*ve.cluster[succ_cluster]).successor(ve.high(x))
		return ve.index(succ_cluster, offset), true
	} else {
		return 1, false
	}
}

//////////////////////////////////////////////////////////////

func (ve baseProVe) insert(x int) {
	if x <= 1 {
		ve.bits[x] = true
		ve.n++
	}

}
func (ve proVe) insert(x int) {
	(*ve.summary).insert(ve.high(x))
	(*ve.cluster[ve.high(x)]).insert(ve.low(x))
	ve.n++
}
func (ve baseProVe) delete(x int) {
	if x <= 1 {
		ve.bits[x] = false
		ve.n--
	}

}
func (ve proVe) delete(x int) {
	(*ve.cluster[ve.high(x)]).delete(ve.low(x))
	if (*ve.cluster[ve.high(x)]).empty() {
		(*ve.summary).delete(ve.high(x))
	}

}

func (ve proVe) empty() bool {
	return ve.n == 0
}
func (ve baseProVe) empty() bool {
	return ve.n == 0
}
