package tree

////////////////////////////////////////////
type intHeap struct {
	values []int
	size   int
}

func makeintsHeap(ints []int) *intHeap {
	fix_up := constructFixup(ints)
	for i := len(ints) - 1; i > 0; i++ {
		fix_up(i)
	}
	return &intHeap{
		values: ints,
		size:   len(ints),
	}
}
func (heap intHeap) maxValue() int {
	return heap.values[0]
}
func (heap intHeap) Push(i int) {
	heap.size++
	if len(heap.values) > heap.size {
		heap.values[heap.size-1] = i
	} else {
		heap.values = append(heap.values, i)
	}
	constructFixup(heap.values)(heap.Size() - 1)
}
func (heap intHeap) Pop() int {
	val := heap.values[0]
	heap.values[0] = heap.values[heap.size-1]
	heap.size--
	constructFixup(heap.values)(heap.Size() - 1)
	return val
}

func (heap intHeap) Size() int {
	return heap.size
}

///////////////////////////////////////////////////
func (heap intHeap) fixHeap() {
	constructFix(heap.values, heap.Size()-1)(0)
}

///////////////////////////////////////////////////////
func constructParent() func(i int) int {
	_parent := func(i int) int {
		if i == 0 {
			return 0
		}
		return (i - 1) / 2
	}
	return _parent
}
func constructLeftChild() func(int) int {
	_child := func(i int) int {
		return 2*i + 1
	}
	return _child
}
func constructSwap(ints []int) func(int, int) {
	_swap := func(i, j int) {
		if i != j {
			ints[j], ints[i] = ints[i], ints[j]
		}
	}
	return _swap
}
func constructFixup(ints []int) func(int) {
	_swap := constructSwap(ints)
	_parent := constructParent()
	///////////////
	var _up_fix func(int)
	_up_fix = func(i int) {
		if i == 0 || ints[(i-1)/2] >= ints[i] {
			return
		}
		_swap(_parent(i), i)
		_up_fix(_parent(i))
	}
	return _up_fix
}
func constructFix(ints []int, max int) func(int) {
	_swap := constructSwap(ints)
	_child := constructLeftChild()
	var _fix func(int)
	_fix = func(i int) {
		if i >= max {
			return
		}
		child := _child(i)
		if child+1 <= max && ints[child+1] > ints[child] {
			child++
		}
		if ints[child] <= ints[i] {
			return
		}
		_swap(child, i)
		_fix(child)
	}
	return _fix
}
