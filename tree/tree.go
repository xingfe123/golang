package tree

/////////////////////////////////////////////////////////////
type Node struct {
	left, right, parent *Node
}
type IntNode struct {
	left, right, parent *IntNode // true it IntNode
	key                 int
}

func tran(n *Node, dosomething func(*Node)) {
	ch := make(chan *Node)
	ch <- n
	size := 1
	for x := range ch {
		if l := x.left; l != nil {
			ch <- l
			size++
		}
		if r := x.right; r != nil {
			ch <- r
			size++
		}
		go dosomething(x)
		size--
		if size == 0 {
			close(ch)
		}
	}
}

func minimum(cur *IntNode) *IntNode {
	if cur == nil {
		return nil
	}
	for ; cur.left != nil; cur = cur.left {
	}
	return cur
}
func maximum(cur *Node) *Node {
	if cur == nil {
		return nil
	}
	for ; cur.left != nil; cur = cur.right {
	}
	return cur
}

//////////////////////////////////
func (n *IntNode) insert(z *IntNode) {
	var y *IntNode
	x := n
	for x != nil {
		y = x
		if z.key < x.key {
			x = x.left
		} else {
			x = x.right
		}
	}
	z.parent = y
	if y == nil {
		x = z
	} else if z.key < y.key {
		y.left = z
	} else {
		y.right = z
	}
}

func next(n *IntNode) *IntNode {
	if n == nil {
		return nil
	}
	if n.left != nil {
		return minimum(n.right)
	}

	for cur := n.parent; ; cur = cur.parent {
		if cur.left.parent == cur {
			return cur
		}
	}
}
func pervious(n *IntNode) *IntNode {
	if n == nil {
		return nil
	}
	if n.right != nil {
		return minimum(n.left)
	}
	for cur := n.parent; ; cur = cur.parent {
		if cur.right.parent == cur {
			return cur
		}
	}
}

////////////////////////////////////////////////////////////////////////
type IntSetTree struct {
	root *IntNode
	size int
}

func makeIntSetTree() *IntSetTree {
	root := makeIntNode(0)
	root.parent = root
	return &IntSetTree{root: root}
}
func makeIntNode(i int) *IntNode {
	var root *IntNode
	root.parent = nil
	root.right = nil
	root.left = nil
	root.key = i
	return root
}
func makeNode() *Node {
	var root *Node
	root.parent = nil
	root.right = nil
	root.left = nil
	return root
}

func (tree IntSetTree) Add(i int) bool {
	x := tree.root
	z := makeIntNode(i)
	var y *IntNode
	for x != nil {
		y = x
		if z.key < x.key {
			x = x.left
		} else {
			x = x.right
		}
	}
	z.parent = y

	res := false
	if y == nil {
		x = z
	} else if z.key < y.key {
		y.left = z
		res = true
	} else if z.key > y.key {
		y.right = z
		res = true
	}
	return res
}
func (tree IntSetTree) Empty() bool {
	node := tree.root
	return node == node.parent
}
func (tree IntSetTree) Remove(i int) {
	//todo
}

func (tree IntSetTree) Cardinality() int {
	return tree.size
}
func (tree IntSetTree) Clear() {
	root := tree.root
	root.left = nil
	root.right = nil
	root.parent = root
}
func (tree IntSetTree) Contains(i ...int) bool {
	return false
}
func (tree IntSetTree) Equal(other IntSetTree) bool {
	if &tree == &other {
		return true
	}
	return false
}
func (tree IntSetTree) IsSubset(other IntSetTree) bool {
	return false
}
func (tree IntSetTree) IsSuperset(other IntSetTree) bool {
	return other.IsSubset(tree)
}
func (tree IntSetTree) Iter() <-chan int {
	ch := make(chan int)
	go func() {
		x := minimum(tree.root)
		for {
			if x != nil {
				ch <- x.key
				x = next(x)
			} else {
				close(ch)
			}
		}
	}()
	return ch
}

/////////////////////////////////////

func (tree IntSetTree) Difference(other *IntSetTree) *IntSetTree {
	return nil //todo
}
func (tree IntSetTree) SymmetricDifference(other *IntSetTree) *IntSetTree {
	return nil //todo
}

func (tree IntSetTree) Union(other *IntSetTree) *IntSetTree {
	return nil //todo
}
func (tree IntSetTree) Intersect(other *IntSetTree) *IntSetTree {
	return nil //todo
}

/////////////////////////////////////////////////////////////
type HashTable struct {
	Data []int
}

/////
func key(x int) int {
	return x
}
func hash(k int, i int) int {
	return k + i
}
func (h *HashTable) addressInsert(x int) {
	h.Data[key(x)] = x
}
func (h *HashTable) addressDelete(x int) {
	h.Data[key(x)] = x
}

func (h *HashTable) Search(k int) int {
	i := 0
	for {
		j := hash(k, i)
		if h.Data[j] == k {
			return j
		}
		i++
		if h.Data[j] == -1 {
			return 0
		}
	}
}

///////////////////////////////////////////////////////////////////////////
// mid - low >=1
func FindMaxSubArray(B []int) (int, int, int) {
	return make_max_sub_array(B).max()
}

func BinFind(A []int, key int) int {
	low := 0
	high := len(A) - 1
	for low < high {
		mid := (low + high) / 2
		if A[mid] < key {
			low = mid + 1
		} else if key < A[mid] {
			high = mid - 1
		} else {
			return mid
		}
	}
	return -1
}

type Mes struct {
	l, h, s int
}
type max_sub_array struct {
	A   []int
	chs chan Mes
}

func Cout(chi chan int) map[int]int {
	dict := make(map[int]int, 0)
	for x := range chi {
		dict[x]++
	}
	return dict
}

func make_max_sub_array(B []int) *max_sub_array {
	return &max_sub_array{
		A:   B,
		chs: make(chan Mes, len(B)),
	}
}

func (f max_sub_array) max() (int, int, int) {
	go func() {
		go f.find(0, len(f.A))
		close(f.chs)
	}()
	max := <-f.chs
	for m := range f.chs {
		if max.s < m.s {
			max = m
		}
	}
	return max.l, max.h, max.s
}

func (f max_sub_array) find(low, high int) {
	mid := (low + high) / 2
	go f.find(low, mid)
	go f.find(mid+1, high)
	go f.findCross(low, mid, high)
}

func (f max_sub_array) findCross(low, mid, high int) {
	left_sum := f.A[mid]
	sum := left_sum
	max_left := mid
	for i := mid - 1; i >= low; i-- {
		sum += f.A[i]
		if sum > left_sum {
			left_sum = sum
			max_left = i
		}
	}
	if low == high {
		f.chs <- Mes{
			l: max_left,
			h: max_left,
			s: left_sum,
		}
	}
	right_sum := f.A[mid+1]
	sum = right_sum
	max_right := mid + 1
	for i := mid + 2; i <= high; i++ {
		sum += f.A[i]
		if sum > left_sum {
			right_sum = sum
			max_right = i
		}
	}
	f.chs <- Mes{
		l: max_left,
		h: max_right,
		s: left_sum + right_sum,
	}
}

/////////////////
type Mat struct {
}

func MakeMat(r, c int) (C *Mat) {
	return &Mat{}
}
func (m Mat) Rows() int {
	return 1
}
func (m Mat) At(i, j int) int {
	return 1
}
func (m Mat) Set(i, j, k int) {
	//todo
}
func (m Mat) AddSet(i, j, k int) {
	//todo
}
func Multiple(A, B *Mat) (C *Mat) {
	n := A.Rows()
	C = MakeMat(n, n)
	ok := make(chan bool)
	for i := 0; i < n; i++ {
		for j := 0; j < n; j++ {
			go func() {
				C.Set(i, j, 0)
				for k := 0; k < n; k++ {
					C.AddSet(i, j, A.At(i, k)*B.At(k, j))
				}
				ok <- true
			}()
		}
	}

	for i := 0; i < n*n; i++ {
		<-ok
	}
	return C
}
