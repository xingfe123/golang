package tree

type BinomialHeap struct {
	root *BinomialNode
	size int
}

func (bh BinomialHeap) Insert(value int) {
	bh.size++
	bh.insert(NewBinomialNode(value))
}
func (bh BinomialHeap) insert(x *BinomialNode) {

}
func (bh BinomialHeap) Pop() int {
	bh.size--
	minnode := getMinimumNode(bh.root)
	removeFromLinkedList(bh.root, minnode)
	//for _, child := range nodeIterator(minnode.children_head) {
	//	removeFromLinkedList(minnode.child_head, child)
	//	bh.insert(child)
	//}
	return minnode.key
}
func (bh *BinomialHeap) Peek() int {
	//return getMinimumNode(bh.root).value
	return 0
}
func (bh *BinomialHeap) Size() int {
	return bh.size
}

type BinomialNode struct {
	key                              int
	parent, child_head, rightsibling *BinomialNode
	order                            int
}

func insertIntoLinkedList(head, node *BinomialNode) *BinomialNode {
	var prev, next *BinomialNode
	prev = nil
	next = head
	for next != nil && node.order < next.order {
		prev, next = next, next.rightsibling
	}
	if prev == nil && next == nil {
		// linked list is empty
		return node
	} else if prev == nil && next != nil {
		// linked list is not empty,
		node.rightsibling = head
		return node
	} else if prev != nil && next == nil {
		// linked list is not empty,
		prev.rightsibling = node
		return head
	} //if prev != nil && next != nil {
	prev.rightsibling = node
	node.rightsibling = next
	return head

}
func removeFromLinkedList(head, node *BinomialNode) *BinomialNode {
	leftsib := getLeftsibling(head, node)
	if leftsib == nil {
		head = node.rightsibling
	} else {
		leftsib.rightsibling = node.rightsibling
	}
	//node.rightsibling
	return head
}
func getLeftsibling(head, node *BinomialNode) *BinomialNode {
	if head == node {
		return nil
	}
	check := head
	for check.rightsibling != node {
		check = check.rightsibling
	}
	return check
}
func getNodeWithOrder(head *BinomialNode, order int) *BinomialNode {
	check := head
	for check != nil {
		if check.order == order {
			return check
		}
		check = check.rightsibling
	}
	return nil
}
func getMinimumNode(head *BinomialNode) *BinomialNode {
	min := head
	check := head.rightsibling
	for check != nil {
		if check.key < min.key {
			min = check
		}
		check = check.rightsibling
	}
	return min
}

func NewBinomialNode(value int) *BinomialNode {
	return &BinomialNode{
		key:          value,
		parent:       nil,
		child_head:   nil,
		rightsibling: nil,
		order:        0,
	}
}
func (bn *BinomialNode) adopt(other *BinomialNode) {
	// assumes "other" has no parent and is not present within a linked list.
	//silbling relations
	insertIntoLinkedList(bn.child_head, other)
	//parent relations
	other.parent = bn
}
func (bn *BinomialNode) rogue(other *BinomialNode) {
	removeFromLinkedList(bn.child_head, other)
	bn.parent = nil
}
func LinkNodes(bn, other *BinomialNode) *BinomialHeap {
	if bn.key < other.key {
		bn.order++
		bn.adopt(other)
	}
	return LinkNodes(other, bn)
}
func (bH BinomialHeap) addSubTree(other *BinomialHeap) *BinomialHeap {
	return &bH
}
func mergeTree(p, q *BinomialHeap) *BinomialHeap {
	if p.root.key < q.root.key {
		return p.addSubTree(q)
	}
	return q.addSubTree(q)
}
