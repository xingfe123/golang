package tree

////////////////////
type FibonacciHeap struct {
	min *ELE
	n   int
}
type ELE struct {
	degree               int
	p, child, prev, next *ELE
	mark                 bool
	key                  int
}

func unionELE(x, y *ELE) {
	x.next, y.next = y.next, x
	x.prev, y.prev = y.prev, x
}
func makeHeap() *FibonacciHeap {
	return &FibonacciHeap{
		min: nil,
		n:   0,
	}
}
func unionHeap(x, y *FibonacciHeap) *FibonacciHeap {
	H := makeHeap()
	H.min = x.min
	unionELE(x.min, y.min)
	if y.min != nil {
		if y.min.key < x.min.key {
			H.min = y.min
		}
	}
	H.n = x.n + y.n
	return H
}
func (H FibonacciHeap) insert(x *ELE) {
	x.degree = 0
	x.p = nil
	x.child = nil
	x.mark = false
	if H.min == nil {
		H.min = x
		H.n = 1
	} else {
		unionELE(x, H.min)
		if x.key < H.min.key {
			H.min = x
		}
	}
	H.n++

}
func (f FibonacciHeap) minimum() *ELE {
	return f.min
}

func (H FibonacciHeap) extract_min() {
	z := H.min
	for z != nil {
		// remove
		if z == z.next {
			H.min = nil
		} else {
			H.min = z.next
			H.consolidate()
		}
		H.n--
	}
}

func (H FibonacciHeap) consolidate() {

}
func (H FibonacciHeap) decrease_key(x *ELE, k int) {
	//assigns to element x  the new key k,
	//assume to be no greater than its current key value
	x.key = k
	y := x.p
	if y != nil && x.key < y.key {
		H.cut(x, y)
		H.cascading(y)
	}
	if x.key < H.min.key {
		H.min = x
	}

}
func (H FibonacciHeap) cut(x, y *ELE) {
	// remove x from the child of y, decrementing y.degree
	// add x to the root list of H
	x.p = nil
	x.mark = false
}
func (H FibonacciHeap) cascading(y *ELE) {
	z := y.p
	if z == nil {
		return
	}
	if y.mark == false {
		y.mark = true
	} else {
		H.cut(y, z)
		H.cascading(z)
	}
}
func (f FibonacciHeap) delete(x *ELE) {

}
