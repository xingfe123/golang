package tree

////////////////////////////////////////////////////////////////
func bubbleSort(A []int) {
	//////////////////////////////////
	_big_than := func(i int) bool {
		return A[i] > A[i+1]
	}
	_swap_next := func(j int) {
		A[j], A[j+1] = A[j+1], A[j]
	}
	//////////////////////////////////
	for i := len(A); i > 1; i-- {
		for j := 0; j < i-1; j++ {
			if _big_than(j) {
				_swap_next(j)
			}
		}
	}
}
func selectSort(A []int) {
	//////////////////////////////////
	_swap := func(i, j int) {
		if i != j {
			A[j], A[i] = A[i], A[j]
		}
	}
	//////////////////////////////////
	_max_index := func(size int) int {
		max := 0
		for i := 1; i < size; i++ {
			if A[max] < A[i] {
				max = i
			}
		}
		return max
	}
	////////////////////////////////////////////////
	for size := len(A); size > 1; size-- {
		_swap(_max_index(size), size-1)
	}
}
func countSort(A []int, k int) {
	size := len(A)
	C := make([]int, k, 0)
	for i := 0; i < size; i++ {
		C[A[i]]++
	}
	for i := 1; i < k; i++ {
		C[i] += C[i-1]
	}
	b := make([]int, size)
	for i := size - 1; i > 0; i-- {
		b[C[A[i]]-1] = A[i]
		C[A[i]]--
	}
}

////////////////////////////////////////////////////////////////////////
type IntCount struct {
	key, count int
}

func TopK(iChan chan int, k int) map[int]int {

	part := func(iChan chan int) []chan int {
		mod := rand.Intn(k) + 2
		x := make([]chan int, mod)
		go func() {
			for i := range iChan {
				x[i%mod] <- i
			}
			for i := 0; i < mod; i++ {
				close(x[i])
			}
		}()
		return x
	}

	_topk := func(res map[int]int) map[int]int {
		keys := make([]int, k)
		swap := func(i, j) {
			keys[i], keys[j] = keys[j], keys[i]
		}
		i := 0
		for key, count := range res {
			i++
			if i < k {
				keys[i-1] = key
			} else if i == 10 {
				for j := k - 1; j > 0; j-- {
					if res[keys[j]] >= res[keys[(j-1)/2]] {
						continue
					} else {
						swap(j, (j-1)/2)
					}
				}
			} else if count > res[keys[0]] {
				keys[0] = key
				start := 0
				for {
					child := 2*start + 1
					if child+1 < k && res[keys[child+1]] < res[keys[child]] {
						child++
					}
					if !(child < k && res[keys[i]] < res[keys[child]]) {
						break
					}
					swap(child, i)
					i = child
				}
			}
		}
		vals := make(map[int]int)
		for _, k := range keys {
			vals[key] = res[key]
		}
		return vals
	}
	size := 0
	total := make(map[int]int)
	for i := range iChan {
		size++
		total[i]++
		if size > 1000 {
			iChans := part(iChan)
			for j := 0; j < len(iChans); j++ {
				for key, value := range TopK(iChans, k) {
					total[key] += value
				}
			}
		}
	}
	return _topk(total)
}

///////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////
func radix(i int, j int) int {
	for ; j != 0; i, j = i/10, j-1 {
	}
	return i % 10
}
func countRadixSort(A []int, j, k int) {
	size := len(A)
	c := make([]int, k, 0)
	for i := 0; i < size; i++ {
		c[radix(A[i], j)]++
	}
	for i := 1; i < k; i++ {
		c[i] += c[i-1]
	}
	b := make([]int, size)
	for i := size - 1; i > 0; i-- {
		b[c[radix(A[i], j)]-1] = A[i]
		c[radix(A[i], j)]--
	}
}
func radixSort(A []int, d, k int) {
	for i := 0; i < d; i++ {
		countRadixSort(A, i, k)
	}
}
func heapSort(A []int) {
	swap := constructSwap(A)

	fix := constructFix(A, len(A)-1)

	for i := 0; i < len(A)/2; i++ {
		fix(i)
	}
	for last := len(A) - 1; last > 0; last-- {
		swap(last, 0)
		last--
		constructFix(A, last)(0)
	}
}
func mergesort(A []int) {
	_merge := func(left, mid, right int) { // [left,mid] (mid,right]
		buffer := make([]int, mid-left)
		copy(buffer, A, len(buffer))
		for i, j, k := 0, 0, mid+1; j <= mid; i++ {
			if buffer[j] < A[k] {
				A[i], j = buffer[j], j+1
			} else {
				A[i], k = A[k], k+1
			}
		}
	}
	////////////////////////////////////////////////////////////////////////
	var _merge_sort func(int, int)
	//////////////
	_merge_sort = func(left, right int) {
		half := (right - left + 1) / 2
		_merge_sort(left, left+half)
		_merge_sort(left+half+1, right)
		_merge(left, left+half, right)
	}
	////////////////////////////////////////////////////////////////////////
	_merge_sort(0, len(A)-1)
}
func insertSort(A []int) {
	for i := 1; i < len(A); i++ {
		value, j := A[i], i
		for j-1 >= 0 && value < A[j-1] {
			A[j-1], j = A[j], j-1
		}
		A[j] = value
	}
}

/////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
func quicksort(A []int) {
	done := make(chan bool)
	var _quicksort func(int, int)
	var _swap func(int, int)
	//////////////////////////////////////////////////
	_swap = func(i, j int) {
		if i != j {
			A[i], A[j] = A[j], A[i]
		}
	}
	_quicksort = func(left, right int) {
		if right-left <= 0 {
			return
		}
		less := 0
		for j := left; j < right; j++ {
			if A[j] <= A[right] {
				_swap(left+less, j)
				less++
			}
		}
		_swap(left+less, right)
		go func() {
			_quicksort(left, left+less-1)
			_quicksort(left+less+1, right)
			if right-left+1 == len(A) {
				done <- true
			}
		}()
	}
	////////////////////////////////////////////
	go _quicksort(0, len(A)-1)
	<-done
}

//////////////////////////////////////////////////////////////////////////

func copy(dest, src []int, size int) {
	for i := 0; i < size; i++ {
		dest[i] = src[i]
	}
}

/////////////////////////////////////////////////////////////////////////////
