package main

type RsaPubKey struct {
	e, n int64
}
type RsaPriKey struct {
	d, n int64
}

func des_encipher(plaintext, ciphertext, key []int64) {

}
func des_decipher(plaintext, ciphertext, key []int64) {

}
func interpol(x, fx, z, pz []float64, n, m int) {
	table := make([]float64, n)
	coeff := make([]float64, n)
	for i := 0; i < n; i++ {
		table[i] = fx[i]
	}
	coeff[0] = table[0]
	for k := 1; k < n; k++ {
		for i := 0; i < n-k; i++ {
			table[i] = (table[i+1] - table[i]) / (x[i+k] - x[i])
		}
		coeff[k] = table[0]
	}
	for k := 0; k < m; k++ {
		pz[k] = coeff[0]
		for j := 1; j < n; j++ {
			term := coeff[j]
			for i := 0; i < j; i++ {
				term = term * (z[k] - x[i])
			}
			pz[k] = pz[k] + term
		}
	}
}
func lsqe(x, y []float64) (float64, float64) {
	sumx, sumy, sumx2, sumxy := 0.0, 0.0, 0.0, 0.0
	for i := 0; i < len(x); i++ {
		sumx += x[i]
		sumy += y[i]
		sumx2 += x[i] * x[i]
		sumxy += x[i] * y[i]
	}
	n := float64(len(x))
	b1 := (sumxy - (sumx*sumy)/n) / (sumx2 - sumx*sumx/n)
	b0 := (sumy - (b1 * sumx)) / n
	return b1, b0
}
func root(f, g func(float64) float64, n int, delta float64) float64 {
	for i, x := 0, 0.0; i < n; i++ {
		x_ := x - f(x)/g(x)
		if x_-x < delta {
			return x_
		}
		x = x_
	}
	return 0.0
}

type HuffNode struct {
	symbol int
	freq   int
}
type HuffCode struct {
	used, code, size int
}

func bit_set(bits []uint64, pos int, b bool) {
	i := pos / 64
	rem := uint(pos - i*64)
	if i > len(bits)-1 {
		bits = append(bits, 0)
	}
	if b {
		bits[i] |= 0x1 << rem
	} else {
		bits[i] &= (0x1 << rem)
	}
}
func bit_get(bits []int64, pos int) bool {
	i := pos / 64
	rem := uint64(pos - i*64)
	b := bits[i]
	if b>>rem&01 == 1 {
		return true
	}
	return false
}
func huffman_compress(original, compresed []int, size int) {

}
