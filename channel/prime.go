package main

import "fmt"
import "runtime"
import "time"
import "os"

func sq(in <-chan int) <-chan int {
	out := make(chan int)
	go func() {
		for n := range in {
			out <- n * n
		}
		close(out)
	}()
	return out
}

func createPrimeChan(max int) chan int {
	return createPrimeChanN(max, runtime.NumCPU())
}
func expmod(base, exp, m int) int {
	if exp == 0 {
		return 1
	} else if exp%2 == 0 {
		x := expmod(base, exp/2, m)
		return (x * x) % m
	}
	return (base * expmod(base, exp/2, m)) % m
}

func createPrimeChanN(max, numCPU int) chan int {
	ch := make(chan int)
	t := numCPU
	ps := []int{2, 3}
	_update := func(i int, x, y []int) {
		x[i], y[i] = y[i]-x[i], x[i]
		if x[i] > y[i] {
			x[i], y[i] = y[i], x[i]
		}
	}
	_swap := func(i, p int, x, y []int) {
		y[i], y[p], x[i], x[p], i =
			y[p], y[i], x[p], x[i], p
	}
	_fixHeap := func(size int, x, y []int) {
		for i := 0; i < size; {
			index := i
			left := 2*i + 1
			right := 2*i + 2
			if left < size {
				_update(index, x, y)
				_update(left, x, y)
				if x[left] < x[index] {
					index = left
				}
			}
			if right < size {
				_update(index, x, y)
				_update(right, x, y)
				if x[left] < x[index] {
					index = left
				}
			}
			if index == i {
				break
			}
			_swap(i, index, x, y)
			i = index
		}
	}

	_is_prime_gcd := func() {
		x := make([]int, size)
		y := make([]int, size)
		for i := 0; i < size; i++ {
			x[i] = current - ps[start+i]
			y[i] = ps[start+i]
			if x[i] > y[i] {
				x[i], y[i] = y[i], x[i]
			}
		}
		for i := size - 1; i > 0; {
			_update(i, x, y)
			p := (i - 1) / 2
			_update(p, x, y)
			if x[i] < x[p] {
				_swap(i, p, x, y)
			}
			break
		}
		for size > 0 {
			_update(0, x, y)
			if y[0] == 1 {
				x[0], y[0] = x[size-1], y[size-1]
				size--
			} else if x[0] == 0 {
				cb <- false
			}
			//fmt.Println(x[0], y[0], current)
			_fixHeap(size, x, y)
		}
		cb <- true
	}
	var is_prime chan bool
	is_prime_gcd := func(cur int) {
		chunk := len(ps) / t
		cb := make(chan bool, t)
		for i := 0; i < t-1; i++ {
			go _is_prime_gcd(i*chunk, chunk, cur, cb)
		}
		go _is_prime_gcd(chunk*(t-1), len(ps)-chunk*(t-1), cur, cb)
		for i := 0; i < t; i++ {
			if <-cb == false {
				is_prime <- false
				close(is_prime)
			}
		}
		is_prime <- true
		close(is_prime)
	}
	is_fast_prime := func(cur int) {
		for k := 0; k < 5; k++ {
			a := random(n-1) + 1
			if expmod(a, cur, cur) != a {
				is_prime <- false
				close(is_prime)
			}
		}
	}
	go func() {
		for i := 5; i < max; i += 2 {
			is_prime = make(chan bool)
			go is_fast_prime(i)
			go is_prime_gcd(i)
			b := <-is_prime
			if b {
				ps = append(ps, i)
				ch <- i
			}

		}
		close(ch)
	}()
	return ch
}

func main() {
	start := time.Now()

	out, err := os.Create("test.primes")
	if err != nil {
		fmt.Println(time.Now().Sub(start).Seconds())
		panic(err)
	}
	defer out.Close()
	for j := 2; j < 20; j++ {
		for i := range createPrimeChanN(40000, j) {
			out.WriteString(string(i))
		}
		fmt.Println(j, time.Now().Sub(start).Seconds())
		start = time.Now()
	}

}
