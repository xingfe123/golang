package main

import (
	"fmt"
	"runtime"
)

type PrimeConstruct struct {
	ps      []int
	size    int
	current int
	gosize  int
}

func (con PrimeConstruct) gcd(x, y int) int {
	for {
		if x > y {
			x, y = y, x
		}
		if x == 0 {
			return y
		}
		for left, right := 0, len(con.ps)-1; left <= right; {
			mid := (left + right) / 2
			if con.ps[mid] == x {
				return 1
			} else if con.ps[mid] < x {
				left = mid + 1
			} else {
				right = mid - 1
			}
		}
		x, y = y-x, x
	}
}

func writeFibonacciToChan(size int, ch chan int) {
	go fibonacciToChan(size, ch)
}
func fibonacciToChan(size int, ch chan int) {
	ch <- 1
	ch <- 1
	values := [2]int{1, 1}
	for i := 2; i < size; i++ {
		values[i&1] = values[(i-1)&1] + values[(i-2)&1]
		ch <- values[i&1]
	}
	close(ch)
}
func makePrimeConstruct(max int) *PrimeConstruct {
	return &PrimeConstruct{
		ps:      []int{2, 3},
		current: 5,
		size:    max,
		gosize:  runtime.NumCPU(),
	}
}
func writePrimeToChan(size int, c chan int) {
	l := makePrimeConstruct(size)
	go l.writeToChan(c)
}
func (con PrimeConstruct) writeToChan(c chan int) {
	for ; con.current < con.size; con.current += 2 {
		if con.isPrime() {
			c <- con.current
			con.ps = append(con.ps, con.current)
		}
	}
	close(c)
}
func (con PrimeConstruct) _is_prime(gos int, pre chan bool) {
	for i := gos; i < len(con.ps); i += con.gosize {
		//if con.gcd(con.current, con.ps[i]) != 1 {
		if con.current%con.ps[i] == 0 {
			pre <- false
			return
		}
	}
	pre <- true
}

func (con PrimeConstruct) isPrime() bool {
	pres := make([]chan bool, con.gosize)
	for i := 0; i < con.gosize; i++ {
		pres[i] = make(chan bool)
		go con._is_prime(i, pres[i])
	}
	for i := 0; i < con.gosize; i++ {
		c := <-pres[i]
		close(pres[i])
		if c == false {
			return false
		}
	}
	return true

}
func copyChan(x, y chan int, cl chan bool) {
	for {
		c, ok := <-x
		if !ok {
			cl <- true
		} else {
			y <- c
		}

	}
}
func makeBoolChans(size int) []chan bool {
	cl := make([]chan bool, size)
	for i := 0; i < size; i++ {
		cl[i] = make(chan bool)
	}
	return cl
}
func makeIntChans(size int) []chan int {
	cl := make([]chan int, size)
	for i := 0; i < size; i++ {
		cl[i] = make(chan int)
	}
	return cl
}
func merge(x, y, z chan int) {
	ch := makeBoolChans(2)
	go copyChan(x, z, ch[0])
	go copyChan(y, z, ch[1])
	if true == <-ch[0] && true == <-ch[1] {
		close(z)
	}
}
func primes(size int) chan int {
	ch := make(chan int)
	go writePrimeToChan(size, ch)
	return ch
}
func main2() {
	//ch := makeIntChans(3)
	//go writePrimeToChan(100, ch[1])
	//go writeFibonacciToChan(90, ch[2])
	//go merge(ch[1], ch[2], ch[0])
	//for c := range ch[0] {
	//	fmt.Println(c)
	//}
	for c := range primes(100) {
		fmt.Println(c)
	}
}

///////////////////////////////////////
