package graph

import (
)

type DenseGraph struct {
	Vcnt, Ecnt int
	Digraph    bool
	Adj        [][]*Edge
}
type Edge struct {
	V, W  int
	value int
}
func (e *Edge) Show(){
	
}
type Iterator struct {
	G    *DenseGraph
	i, v int
}

/////////////////////////////////////////////////////////////////////////////
type MST struct {
	G       *DenseGraph
	wt      []int
	fr, mst []*Edge
}


func (m *MST) pfs(s int) {

}
func (m *MST) Show() {
	for v := 1; v < m.G.V(); v++ {
		if m.mst[v] == nil {
			m.mst[v].Show()
		}
	}
}

////////////////////////////////////////////////////////////////////////////
func CreateIterator(g *DenseGraph, v int) *Iterator {
	return &Iterator{
		G: g,
		v: v,
		i: -1,
	}
}
func (iter *Iterator) begin() *Edge {
	return iter.next()
}
func (iter *Iterator) next() *Edge {
	j := iter.i
	j++
	for ; j < iter.G.V(); j++ {
		if iter.G.get(iter.v, j) != nil {
			return iter.G.Adj[iter.v][j]
		}
	}
	return nil
}
func (iter *Iterator) end() bool {
	return iter.i >= iter.G.V()
}

//////////////////////////////////////////////////////
func CreateGraph(v int) *DenseGraph {
	graph := &DenseGraph{
		Vcnt: v,
		Ecnt: 0,
		Adj:  make([][]*Edge, v),
	}
	for i := 0; i < v; i++ {
		graph.Adj[i] = make([]*Edge, v)
	}
	return graph
}

func (g *DenseGraph) V() int {
	return g.Vcnt
}

func (g *DenseGraph) E() int {
	return g.Ecnt
}

func (g *DenseGraph) Directed() bool {
	return g.Digraph
}

func (g *DenseGraph) Insert(e *Edge) {
	v := e.V
	w := e.W
	if g.get(v, w) == nil {
		g.Ecnt++
	}
	g.set(v, w, e)
	if !g.Digraph {
		g.set(w, v, e)
	}
}

func (g *DenseGraph) Remove(e *Edge) {
	v := e.V
	w := e.W
	if g.get(v, w) == nil {
		g.Ecnt--
	}
	g.set(v, w, nil)
	if !g.Digraph {
		g.set(w, v, nil)
	}
}

func (g *DenseGraph) set(v, w int, e *Edge) {
	g.Adj[v][w] = e
}

func (g *DenseGraph) get(v, w int) (e *Edge) {
	return g.Adj[v][w]
}

////////////////////////////////////////////
