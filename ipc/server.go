package ipc

import (
	"encoding/json"
	"fmt"
)

type Request struct {
	Method string `json:method`
	Params string `json:params`
}
type Response struct {
	Code string `json:code`
	Body string `json:body`
}
type Server struct {
}
type IpcServer struct {
	Server
}

func NewIpcServer(server Server) *IpcServer {
	return &IpcServer{server}
}
func (server *IpcServer) Connect() chan string {
	session := make(chan string, 0)
	go func(c chan string) {
		for {
			re := <-c
			if re == "CLOSE" {
				break
			}
		}
	}(session)
	fmt.Println("A new session has been created successfully.")
	return session
}
