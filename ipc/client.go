package ipc

import (
	"encoding/json"
)

type IpcClient struct {
	conn chan string
}

func (client IpcClient) Call(method, params string) (
	resp *Response, err error) {
	req := &Request{method, params}
	var b []byte
	b, err = json.Marshall(req)
	if err != nil {
		return
	}
}
