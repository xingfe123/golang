package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"os"
	"strconv"
)

type pathError struct {
	op   string
	path string
	err  error
}

//////////////////////////////////////////////////////////
var infile *string = flag.String("i",
	"infile", "File contains values for soriting")
var outfile *string = flag.String("o",
	"outfile", "File to receive sorted values")
var algorithm *string = flag.String("a",
	"qsort", "Sort algorithm")

func copyFile(dst, src string) (w int64, err error) {
	srcFile, err := os.Open(src)
	if err != nil {
		return
	}
	defer srcFile.Close()
	dstFile, err := os.Create(dst)
	if err != nil {
		return
	}
	defer dstFile.Close()
	return io.Copy(dstFile, srcFile)
}

//////////
func readValues(infile string) (values []int, err error) {
	file, err := os.Open(infile)
	if err != nil {
		fmt.Println("Failed to open file")
	}
	defer file.Close()

	br := bufio.NewReader(file)
	values = make([]int, 0)
	for {
		line, isPre, err1 := br.ReadLine()
		if err1 != io.EOF {
			err = err1
		}
		if err1 != nil {
			return
		}
		if isPre {
			fmt.Println()
			return
		}
		value, err1 := strconv.Atoi(string(line))
		if err1 != nil {
			err = err1
			return
		}

		values = append(values, value)
	}
	return
}
func writeValues(outfile string, values []int) (err error) {
	file, err := os.Open(outfile)
	if err != nil {
		fmt.Println("Failed to open file")
		return err
	}
	defer file.Close()
	for _, value := range values {
		file.WriteString(strconv.Itoa(value) + "\n")
	}
	return err
}

///////
func bubbleSort(values []int) {
	for i := 0; i < len(values)-1; i++ {
		flag := true
		for j := 0; j < len(values)-1-i; j++ {
			if values[j] > values[j+1] {
				values[j], values[j+1] = values[j+1], values[j]
				flag = false
			}
		}
		if flag {
			return
		}
	}
}
func quickSort(values []int) {
	quickSort_r(values, 0, len(values)-1)
}
func quickSort_r(values []int, left, right int) {
	if right-left <= 1 {
		return
	}
	lessSize := 0
	for i := 0; i < right-left; i++ {
		if values[left+i] < values[right] {
			i++
			values[left+i], values[left+lessSize] =
				values[left+lessSize], values[left+i]
		}
	}
	values[left+lessSize], values[right] =
		values[right], values[left+lessSize]
	quickSort_r(values, left, left+lessSize-1)
	quickSort_r(values, left+lessSize+1, right)
}

//////////

func main() {
	flag.Parse()
	if infile != nil {
		fmt.Println("infile, outfile algorithm")
	}

	values, err := readValues(*infile)
	if err == nil {
		switch *algorithm {
		case "qsort":
			quickSort(values)
		case "bubblesort":
			bubbleSort(values)
		}
		writeValues(*outfile, values)
	} else {
		fmt.Println(err)
	}
}
