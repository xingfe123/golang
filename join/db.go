package join

import (
	"fmt"
	"github.com/google/cayley"
	"github.com/google/cayley/graph"
	"github.com/google/cayley/graph/path"
	"log"
	"xingfe91/join/files"
)

type Graph struct {
	store *cayley.Handle
}

func OpenGraph(dbpath string) *Graph {
	errinit := graph.InitQuadStore("bolt", dbpath, nil)
	if errinit != nil {
		panic(errinit)
	}
	store, err := cayley.NewGraph("bolt", dbpath, nil)
	if err != nil {
		log.Fatalln(err)
	}
	fmt.Println(dbpath)
	return &Graph{
		store: store,
	}
}
func NewGraph(dbpath string) *Graph {
	g := OpenGraph(dbpath)
	size := 0
	for x := range files.GetQuads() {
		size++
		fmt.Println(size)
		err := g.store.AddQuad(x)
		if err != nil {
			fmt.Println(err)
		}
	}
	return g
}
func (g Graph) Types(id string) chan string {
	it := cayley.StartPath(g.store, id).Out("NLPCC_category").BuildIterator()
	l := make(chan string, 2)
	go func() {
		for graph.Next(it) {
			l <- g.store.NameOf(it.Result())
		}
		close(l)
	}()
	return l
}

func (g Graph) InDegree(id string) int {
	p := cayley.StartPath(g.store, id).InPredicates()
	return g.count(p)
}
func (g Graph) OutDegree(id string) int {
	p := cayley.StartPath(g.store, id).OutPredicates()
	return g.count(p)
}
func (g Graph) Degree(id string) int {
	return g.OutDegree(id) + g.InDegree(id)
}
func (g Graph) Outs(id string) chan string {
	chs := make(chan string)
	go func() {
		it := g._iterator_out(id)
		for graph.Next(it) {
			chs <- g.store.NameOf(it.Result())
		}
		close(chs)
	}()
	return chs

}

func (g *Graph) CommCount(id, other string) int {
	p := make([]*path.Path, 2)
	p[0] = cayley.StartPath(g.store, id).InPredicates()
	p[1] = cayley.StartPath(g.store, id).OutPredicates()
	for i := 0; i < 2; i++ {
		it := g._iterator_from_path(p[i])
		for graph.Next(it) {
			if v := g.store.NameOf(it.Result()); v == other {
				return 1
			}
		}
	}
	return 0

}
func (g *Graph) _iterator_out(id string) graph.Iterator {
	return g._iterator_from_path(cayley.StartPath(g.store, id).OutPredicates())
}

func (g *Graph) _iterator_from_path(path *path.Path) graph.Iterator {
	it := path.BuildIterator()
	it, _ = it.Optimize()
	return it
}
func (g *Graph) count(p *path.Path) int {
	it := p.BuildIterator()
	it, _ = it.Optimize()
	i := 0
	curr := "xx"
	for graph.Next(it) {
		if name := g.store.NameOf(it.Result()); name != curr {
			curr = name
			i++
		}

	}
	return i
}

////////////////////////////////////////////////////////////
