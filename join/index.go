package join

import (
	"fmt"
	"github.com/blevesearch/bleve"
	"io/ioutil"
	"log"
	"strconv"
	"xingfe91/join/files"
	"xingfe91/join/types"
)

var (
	dir      = "/mnt/windows/NLPCC2015.releaseRDF/releaseRDF.dumps.20150427/"
	dumpPath = dir + "releaseRDF.dumps"
)

type LabelGenerator struct {
	_index bleve.Index
	_db    *IdNickNameMap
}

func OpenLabelGenerator(path string) *LabelGenerator {
	index := OpenIndex(path + ".ir")
	db := OpenNickNameMap(path + ".nick")

	return &LabelGenerator{
		_index: index,
		_db:    db,
	}
}
func (lg LabelGenerator) getlabels(x string) chan *DocLabel {
	res := make(chan *DocLabel, 0)
	getNoConfitRanges(lg.Query(x), x, res)
	return res
}

////////////////////////////////////////////////////
type DocLabel struct {
	doc    string
	labels []*WordLabel
}

func (ld DocLabel) Len() int {
	return len(ld.labels)
}

///////////////////////////////////////////
type WordLabel struct {
	_range types.Range
	_word  string
	_id    string
}

func (wl WordLabel) equal(other *WordLabel) bool {
	return wl._id == other._id
}
func (wl WordLabel) Confict(other *WordLabel) bool {
	x := wl._range
	y := other._range
	if x.Left > y.Left {
		x, y = y, x
	}
	if x.Right < y.Left {
		return false
	}
	return true
}
func check(words []*WordLabel) bool {
	var _check func(int, int) bool
	_check = func(start, size int) bool {
		if size <= 1 {
			return true
		}
		for i := 0; i < size-1; i++ {
			if words[size-1].Confict(words[i]) {
				return false
			}
		}
		half := size / 2
		return _check(start, half) &&
			_check(start+half, size-half-1)
	}
	return _check(0, len(words))
}

func getNoConfitRanges(ranges []*WordLabel,
	x string, res chan *DocLabel) {
	vals := make([][]*WordLabel, len(ranges))
	size := 0
	vals[size] = ranges[:]
	size++
	var words []*WordLabel
	for size > 0 {
		words, size = vals[size-1], size-1
		if !check(words) {
			continue
		}
		res <- &DocLabel{
			doc:    x,
			labels: words,
		}
		swap := func(i, j int) {
			words[i], words[j] = words[j], words[i]
		}
		for i := 0; i < len(words); i++ {
			swap(i, len(words)-1)
			vals[size] = ranges[0 : len(words)-1]
			size++
			swap(len(words)-1, i)
		}
	}
	close(res)
}
func (inf LabelGenerator) Generate(x string) []*DocLabel {
	res := make(chan *DocLabel, 8)
	docs := make([]*DocLabel, 8)
	go getNoConfitRanges(inf.Query(x), x, res)
	for x := range res {
		docs = append(docs, x)
	}
	return docs
}
func (inf LabelGenerator) Query(x string) []*WordLabel {
	query := bleve.NewQueryStringQuery(x)
	searchRequest := bleve.NewSearchRequest(query)
	searchResult, _ := inf._index.Search(searchRequest)
	datas := make([]*WordLabel, 0)
	if size := len(searchResult.Hits); size != 0 {
		for i := 0; i < size; i++ {
			dataid := searchResult.Hits[i].ID
			idname := inf._db.GetData(dataid)
			datas = append(datas, &WordLabel{
				_word:  idname.Name,
				_id:    idname.Id,
				_range: types.StringRange(x, idname.Name)})
		}
	}
	return datas
}

///////////////////////////////////////////////////////////////
func NewNickNameIndex(path string) *LabelGenerator {
	index := OpenIndex(path + ".ir")
	db := OpenNickNameMap(path + ".nick")
	paths, err := ioutil.ReadDir(files.Nickdir)
	if err != nil {
		panic(err)
	}
	size := 0
	done := make(chan bool, 3)
	for _, f := range paths {
		for u := range files.MakeUChanFromPath14(files.Nickdir + f.Name()) {
			size++
			go func() {
				message := types.Message{
					Id:   strconv.Itoa(size),
					Name: u.N(),
				}
				db.AddNickName(message.Id, u.Id(), u.N())
				err := index.Index(message.Id, message)
				if err != nil {
					fmt.Println(err)
				}
				done <- true
			}()
		}
	}
	for ; size > 0; size-- {
		<-done
	}
	return &LabelGenerator{
		_index: index,
		_db:    db,
	}
}

/////////////////////////////////////////////////////////////

func OpenIndex(path string) bleve.Index {
	index, err := bleve.Open(path)
	if err == bleve.ErrorIndexPathDoesNotExist {
		log.Printf("Creating new index...")
		// create a mapping
		indexMapping := bleve.NewIndexMapping()
		index, err = bleve.New(path, indexMapping)
		if err != nil {
			log.Fatal(err)
		}
	} else if err == nil {
		log.Printf("Opening existing index...")
	} else {
		log.Fatal(err)
	}
	return index

}
func newSearchRequest(q bleve.Query) *bleve.SearchRequest {
	return bleve.NewSearchRequestOptions(q, 500000, 0, false)
}

func query(index bleve.Index, q string) chan int {
	ch := make(chan int)
	go func() {
		query := bleve.NewQueryStringQuery(q)
		searchRequest := newSearchRequest(query)
		searchResult, _ := index.Search(searchRequest)
		if size := len(searchResult.Hits); size != 0 {
			for i := 0; i < size; i++ {
				t, err := strconv.Atoi(searchResult.Hits[i].ID)
				if err != nil {
					fmt.Println(err, size)
				} else {
					ch <- t
				}
			}
		}
		close(ch)
	}()
	return ch
}

//////////////////////
func IndexTest(index bleve.Index) {
	count, errCount := index.DocCount()
	if errCount != nil {
		panic(errCount)
	}
	fmt.Println(count)
}

////////////////////////////////////
// deprecated
