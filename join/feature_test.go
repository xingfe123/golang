package join

import (
	"fmt"
	_ "github.com/google/cayley/graph/bolt"
	//"github.com/blevesearch/bleve"

	"testing"
	"xingfe91/join/files"
	//"xingfe91/join/types"
)

func _Test1(t *testing.T) {
	dict := readWordDict(files.WordsFile)
	max := 0
	for _, v := range dict.dict {
		//fmt.Println(k, v)
		if max < v {
			max = v
		}
	}
	fmt.Println(max)
}

//////////////////////////////////////////////////////////////

func _TestCalcutator(t *testing.T) {
	dict := readWordDict(files.TypesFile)
	fmt.Println(dict.Size())
	///////////////////////
	golds, _ := GetGoldDocLabel()
	calc := OpenDefaultFeatureCalculator()
	i := 0
	for k, v := range golds {
		if i < 2 {
			vec := calc.Eval(v)
			fmt.Println(k, vec.Len(), calc.NerNumber())
		}
		i++
	}
}
func _TestGraph(t *testing.T) {
	graph := OpenGraph(files.GraphDir)
	if graph == nil {
		panic("error")
	}
	fmt.Println(graph.InDegree("98614"))
	for x := range graph.Types("98614") {
		fmt.Println(x)
	}
	//fmt.Println(graph.InDegree("22933"))
	//fmt.Println(graph.OutDegree("22931"))
	//fmt.Println(graph.OutDegree("22933"))

}
