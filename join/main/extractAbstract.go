package main

import (
	"fmt"
	"os"
	"strings"
	"xingfe91/join"
	"xingfe91/join/files"
	"xingfe91/join/types"
)

func extractAbstract(to, from string) {
	absIndex := 4
	tofile, err := os.Create(to)
	if err != nil {
		panic(err)
	}
	defer tofile.Close()
	i := 0
	for line := range files.MakeLineChanFromPath(from) {
		lines := strings.Split(line, "\t")

		if len(lines) < 6 || lines[absIndex] != "abstract" {
			continue
		}
		abs := strings.TrimSpace(lines[5])
		if abs == "" {
			continue
		}
		_, err :=
			tofile.WriteString(lines[0] + "\t" + abs + "\n")
		if err != nil {
			fmt.Println(err)
			break
		}
		fmt.Println(i)
		i++
	}
}

func extractNLPCCTypes(dest, src string) {
	wordChan := make(chan string, 100)
	go files.WriteWordSetChan(dest, wordChan)
	for line := range files.MakeLineChanFromPath(src) {
		lines := strings.Split(line, "\t")
		if len(lines) < 4 || lines[4] != "NLPCC_category" {
			continue
		} else if abs := strings.TrimSpace(lines[5]); len(abs) > 0 {
			wordChan <- lines[5]
		}
	}
	close(wordChan)
}

func extractWords(dest, src string) {
	wordChan := make(chan string, 100)
	go files.WriteWordSetChan(dest, wordChan)
	for line := range files.MakeLineChanFromPath(src) {
		lines := strings.Split(line, "\t")
		if len(lines) < 4 {
			fmt.Println(lines)
			continue
		} else if words := strings.TrimSpace(lines[2]); len(words) > 0 {
			for _, x := range strings.Split(words, "") {
				wordChan <- x
			}
		}
	}
	close(wordChan)
}

func indexAbstract(dir, file string) {
	index := join.OpenIndex(dir)
	size := 0
	for u := range files.MakeUChanFromPath14(file) {
		size++
		message := types.Message{
			Id:   u.Id(),
			Name: u.N(),
		}
		index.Index(message.Id, message)
		fmt.Println(size)
	}
	join.IndexTest(index)
}
