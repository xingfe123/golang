package main

import (
	//"flag"
	"fmt"
	//	"github.com/google/cayley/graph"
	_ "github.com/google/cayley/graph/bolt"
	"os"
	//	"strings"
	"xingfe91/join"
	"xingfe91/join/files"
	//	"xingfe91/join/types"
)

func main() {
	if len(os.Args) >= 2 {
		switch os.Args[1] {
		case "extractAbs":
			extractAbstract(files.AbstractFile, files.DumpsFile)
		case "newGraph":
			join.NewGraph(files.GraphDir)
		case "nickNameIndex":
			join.NewNickNameIndex("/tmp/test")
		case "indexAbs":
			indexAbstract(files.AbstractIndexDir, files.AbstractFile)
		case "extractWords":
			extractWords(files.WordsFile, files.GoldFile)
		case "extractTypes":
			extractNLPCCTypes(files.TypesFile, files.DumpsFile)
		}
	} else {
		//join.NewGraph("/tmp/graph")
		fmt.Println("/tmp/types")
		join.ExtractTypes("/tmp/types")
		//extractWords("/tmp/words", files.GoldFile)
		//
		//ir.CreateNickNameIndex("/tmp/nickname")
		//indexAbstract("/tmp/abstract", files.AbstractFile)
		//files.AbstractIndexDir
	}

}
