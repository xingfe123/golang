package files

import (
	"bufio"
	"fmt"
	"os"
	"sort"
	"strconv"
	"strings"
	"testing"
)

func Test_conv(t *testing.T) {
	path := Uniqtest
	for line := range MakeLineChanFromPath(path) {
		entitylab := strings.Split(line, "\t")
		id, _ := strconv.Atoi(entitylab[0])
		name := entitylab[1]
		fmt.Println(id, name)
	}
}

func _Test_readline(t *testing.T) {
	path := "/mnt/windows/NLPCC2015.releaseRDF/releaseRDF.dumps.20150427/releaseRDF.dumps"
	identitys := "/mnt/windows/xx"
	outfile, errCreatefile := os.Create(identitys)
	if errCreatefile != nil {
		panic(errCreatefile)
	}
	l := MakeLineChanFromPath(path)
	fmt.Println(path)
	for id := range MakeIdChanFromLineChan(l) {
		//fmt.Println(id)
		outfile.WriteString(strconv.Itoa(id) + "\n")
	}
}
func _Test_clearInt(t *testing.T) {
	file := "/mnt/windows/xx"
	in, err := os.Open(file)
	if err != nil {
		panic(err)
	}
	reader := bufio.NewReader(in)
	var (
		part   []byte
		prefix bool
	)
	values := make([]int, 0)
	for {
		part, prefix, err = reader.ReadLine()
		if err != nil {
			break
		}
		if prefix {
			continue
		}
		id, errCov := strconv.Atoi(string(part))
		if errCov != nil {
			panic(errCov)
		}
		values = append(values, id)
		fmt.Println(len(values))
	}
	sort.Sort(sort.IntSlice(values))
}
