package files

import (
	"bufio"
	"fmt"
	"github.com/google/cayley/quad"
	"io"
	"os"
	"strconv"
	"strings"
)

var (
	GoldFile          = "/home/luoxing/goldData.txt"
	Nickdir           = "/mnt/windows/NLPCC2015.releaseRDF/nickname/"
	DumpsFile         = "/home/luoxing/release.dumps"
	Uniqtest          = "/mnt/windows/NLPCC2015.releaseRDF/nickname/test.txt"
	GraphPathFileTest = "/mnt/windows/NLPCC2015.releaseRDF/releaseRDF.dumps.20150427/head.txt"
	AnnotationTest    = "/mnt/windows/NLPCC2015.releaseRDF/releaseRDF.dumps.20150427/nlpcc_annotation_100.20150427.txt"
)

var (
	AbstractIndexDir = "/home/luoxing/experiment/abstract"
	AbstractFile     = "/home/luoxing/experiment/absfile"
	GraphDir         = "/home/luoxing/experiment/graph"
	WordsFile        = "/home/luoxing/experiment/words"
	TypesFile        = "/home/luoxing/experiment/types"
)

func GetQuads() chan quad.Quad {
	lines := MakeLineChanFromPath(DumpsFile)
	quads := make(chan quad.Quad, 2)
	go _quadChan_from_lineChan(lines, quads)
	return quads
}
func MakeIndexChanFromPath(path string) chan IdName {
	lineChan := MakeLineChanFromPath(path)
	return makeUniq14(lineChan)
}
func MakeUChanFromPath14(path string) chan IdName {
	lineChan := MakeLineChanFromPath(path)
	uChan := make(chan IdName)
	go _lineChan_2_uChan14(lineChan, uChan)
	return uChan
}
func MakeIdChanFromLineChan(lineChan chan string) chan int {
	intChan := make(chan int)
	go _lineChan2idChan(lineChan, intChan)
	return intChan
}
func MakeLineChanFromPath(path string) chan string {
	lineChan := make(chan string)
	go configureChan(path, lineChan)
	return lineChan
}

func Open(path string) (*os.File, error) {
	if _, err := os.Stat(path); err != nil && os.IsNotExist(err) {
		return os.Create(path)
	}
	return os.Open(path)
}
func Create(path string) (*os.File, error) {
	if _, err := os.Stat(path); err != nil && os.IsNotExist(err) {
		os.Rename(path, path+".old")
	}
	return os.Create(path)
}
func WriteWordSetChan(path string, wordChan chan string) {
	tofile, err := Create(path)
	if err != nil {
		panic(err)
	}
	defer tofile.Close()
	wordset := make(map[string]bool)
	for word := range wordChan {
		if wordset[word] == true {
			continue
		}
		wordset[word] = true
		tofile.WriteString(word + "\n")
	}
}

func ReadWordSet(wordset map[string]bool, from string) {
	fromfile, err := Open(from)
	if err != nil {
		panic(err)
	}
	defer fromfile.Close()
	reader := bufio.NewReader(fromfile)
	for {
		part, prefix, errRead := reader.ReadLine()
		if errRead != nil {
			if errRead != io.EOF {
				panic(err)
			}
			break
		}
		if !prefix {
			wordset[string(part)] = true
		}
	}
}

type IdName interface {
	Id() string
	I() int
	N() string
}

///////////////
func configureChan(path string, lineChan chan string) {
	file, err := os.Open(path)
	defer file.Close()
	if err != nil {
		panic("can't open file")
	}
	reader := bufio.NewReader(file)
	for {
		part, prefix, errRead := reader.ReadLine()
		if errRead != nil {
			break
		}
		if !prefix {
			lineChan <- string(part)
		}
	}
	if err == io.EOF {
		err = nil
	}
	close(lineChan)
}

func writeIChanToFile(iChan chan int, path string) {
	out, errOpenWriteFile := os.Open(path)
	defer out.Close()
	if errOpenWriteFile != nil {
		panic(errOpenWriteFile)
	}
	for i := range iChan {
		out.WriteString(string(i) + "\n")
	}
	close(iChan)
}
func writeUChanToFile(uChan chan IdName, path string) {
	out, errOpenWriteFile := os.Open(path)
	defer out.Close()
	if errOpenWriteFile != nil {
		panic(errOpenWriteFile)
	}
	for u := range uChan {
		out.WriteString(string(u.I()) + "\t" + u.N() + "\n")
	}
	close(uChan)
}

func makeUniq14(lineChan chan string) (uChan chan IdName) {
	uChan = make(chan IdName)
	go _lineChan_2_uChan(lineChan, uChan)
	return uChan
}

func _lineChan_2_uChan(lineChan chan string, uChan chan IdName) {
	pre := -1
	for line := range lineChan {
		entitylab := strings.Split(line, "\t")
		id, err := strconv.Atoi(entitylab[0])
		name := string(entitylab[3])
		if err != nil {
			//panic(err)
			fmt.Println(entitylab[0])
		}
		if pre != id {
			uChan <- _id_name{
				_i: id,
				_n: name,
			}
			pre = id
		}
	}
	close(uChan)
}
func _lineChan_2_uChan14(lineChan chan string, uChan chan IdName) {
	pre := -1
	for line := range lineChan {
		entitylab := strings.Split(line, "\t")
		id, err := strconv.Atoi(entitylab[0])
		name := string(entitylab[1])
		if err != nil {
			//panic(err)
			fmt.Println(entitylab[0])
		}
		if pre != id {
			uChan <- _id_name{
				_i: id,
				_n: name,
			}
			pre = id
		}
	}
	close(uChan)
}
func _lineChan2idChan(lineChan chan string, intChan chan int) {
	pre := -1
	for line := range lineChan {
		entitylab := strings.Split(line, "\t")
		id, err := strconv.Atoi(entitylab[0])
		if err != nil {
			panic(err)
			//fmt.Println(entitylab[0])
		}
		if pre != id {
			intChan <- id
			pre = id
		}

	}
	close(intChan)
}

func _quadChan_from_lineChan(lineChan chan string, intChan chan quad.Quad) {
	for line := range lineChan {
		entitylab := strings.Split(line, "\t")
		if len(entitylab) < 6 {
			continue
		}
		prediciate := entitylab[1]
		object := entitylab[2]
		if prediciate == "0" {
			prediciate = entitylab[4]
		}
		if object == "0" {
			object = entitylab[5]
		}

		intChan <- quad.Quad{
			entitylab[0],
			prediciate,
			object,
			"",
		}

	}
	close(intChan)
}

type _id_name struct {
	_i int
	_n string
}

func (x _id_name) I() int {
	return x._i
}
func (x _id_name) N() string {
	return x._n
}

func (x _id_name) Id() string {
	return strconv.Itoa(x._i)
}
