package join

import (
	"xingfe91/join/files"
)

type WordDict struct {
	dict map[string]int
}

func readWordDict(path string) *WordDict {
	_dict := make(map[string]bool)
	dict := make(map[string]int)
	files.ReadWordSet(_dict, path)
	index := 1
	for x, _ := range _dict {
		dict[x] = index
		index++
	}
	//	fmt.Println(index)
	return &WordDict{
		dict: dict,
	}
}
func (wd WordDict) Size() int {
	return len(wd.dict)
}
func (wd WordDict) GetIndex(x string) int {
	return wd.dict[x]
}
