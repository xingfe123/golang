package join

import (
	"fmt"
	"github.com/boltdb/bolt"
	"log"
	"xingfe91/join/types"
)

type IdNickNameMap struct {
	db         *bolt.DB
	ids, names *bolt.Bucket
}

func OpenNickNameMap(path string) *IdNickNameMap {
	db := openKV(path)
	defer closeKV(db)
	ids, names := createIdNameBucket(db)
	return &IdNickNameMap{
		db:    db,
		ids:   ids,
		names: names,
	}
}

func (nick IdNickNameMap) GetData(id string) *types.IdNickName {
	iD := []byte(id)
	return &types.IdNickName{
		Id:   string(nick.ids.Get(iD)),
		Name: string(nick.names.Get(iD)),
	}
}

func (nick IdNickNameMap) AddNickName(ID, id, name string) {
	put(nick.ids, ID, id)
	put(nick.names, ID, name)
}

/////////////////////////////////////////////////////////////////////////////
func put(b *bolt.Bucket, k, v string) {
	err := b.Put([]byte(k), []byte(v))
	if err != nil {
		fmt.Println(err)
	}
}
func openKV(path string) *bolt.DB {
	db, err := bolt.Open(path, 0600, nil)
	if err != nil {
		log.Fatal(err)
	}
	return db
}
func closeKV(db *bolt.DB) {
	err := db.Close()
	if err != nil {
		log.Fatal(err)
	}
}
func createIdNameBucket(db *bolt.DB) (*bolt.Bucket, *bolt.Bucket) {
	var ids, names *bolt.Bucket
	err := make([]error, 3)
	err[2] = db.Update(func(tx *bolt.Tx) error {
		ids, err[0] = tx.CreateBucketIfNotExists([]byte("id"))
		names, err[1] = tx.CreateBucketIfNotExists([]byte("name"))
		for i := 0; i < len(err)-1; i++ {
			if err[i] != nil {
				return err[i]
			}
		}
		return nil
	})
	PrintLnNotNil(err[2])
	return ids, names
}

////////////////////////////////////////////////////////////////////////
