package join

import (
	//"github.com/blevesearch/bleve"
	//"github.com/gonum/matrix/mat64"
	//"fmt"
	"github.com/blevesearch/bleve"
	"github.com/gonum/matrix/mat64"
	"strings"
	"xingfe91/join/files"
)

type FeatureCalculator struct {
	_ners  *WordDict
	_words *WordDict
	_graph *Graph
	_index bleve.Index // no initi
}

//////////

////////////////////////////////////////////////////////////////////////////
func OpenDefaultFeatureCalculator() *FeatureCalculator {
	return makeFeatureCalculator(files.TypesFile,
		files.WordsFile, files.GraphDir)
}
func makeFeatureCalculator(nerp, wordp, graphp string) *FeatureCalculator {
	ners := readWordDict(nerp)
	words := readWordDict(wordp)
	graph := OpenGraph(graphp)
	//nick := OpenLabelGenerator(nickp)
	return &FeatureCalculator{
		_ners:  ners,
		_graph: graph,
		_words: words,
	}
}

func (fc FeatureCalculator) WordNumber() int {
	return fc._words.Size()
}

func (fc FeatureCalculator) Eval(dl *DocLabel) *mat64.Vector {
	curr := make([]float64, fc.NerNumber()+1) //+fc.WordNumber()+1)
	for _, word := range dl.labels {
		res := fc.gner1(word._id)
		//res = append(res, fc.gner2(word._word)...)
		for i := 0; i < len(curr); i++ {
			curr[i] += res[i]
		}
	}
	return mat64.NewVector(len(curr), curr)
}

func (fc FeatureCalculator) NerNumber() int {
	return fc._ners.Size()
}

/////////////////////////////////////////////////////////////////////////
func (fc FeatureCalculator) gner1(id string) []float64 {
	res := make([]float64, fc.NerNumber()+1)
	for z := range fc._graph.Types(id) {
		res[fc._ners.GetIndex(z)] = 1.0
	}
	return res
}
func (fc FeatureCalculator) gner2(word string) []float64 {
	res := make([]float64, fc.WordNumber()+1)
	for _, z := range strings.Split(word, "") {
		res[fc._words.GetIndex(z)] = 1.0
	}
	return res
}
func (fc FeatureCalculator) sim(doc string, datas []*WordLabel) []float64 {
	query := bleve.NewQueryStringQuery(doc)
	searchRequest := bleve.NewSearchRequest(query)
	searchResult, _ := fc._index.Search(searchRequest)
	in := make(map[string]int)
	for i := 0; i < len(datas); i++ {
		in[datas[i]._id] = i + 1
	}
	feature := make([]float64, len(datas))
	if size := len(searchResult.Hits); size != 0 {
		for i := 0; i < size; i++ {
			hit := searchResult.Hits[i]
			if i := in[hit.ID]; i != 0 {
				feature[i-1] = hit.Score
			}
		}
	}
	return feature
}

///////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////
func DocLabelCommSize(x, y DocLabel) (size int) {
	for i := 0; i < x.Len(); i++ {
		for j := i; j < y.Len(); j++ {
			if x.labels[i].equal(y.labels[j]) {
				size++
			}
		}
	}
	return
}
func assessment(x, g []DocLabel) (float64, float64) {
	comSize := 0
	for _, gL := range g {
		for _, xL := range x {
			if xL.doc == gL.doc {
				comSize += DocLabelCommSize(xL, gL)
			}
		}
	}
	gSize := 0
	for _, gL := range g {
		gSize += gL.Len()
	}
	xSize := 0
	for _, xL := range x {
		xSize += xL.Len()
	}
	return float64(comSize) / float64(xSize), float64(comSize) / float64(gSize)
}

////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
