package types

//////////////////////////////////////////////////////////////////////////
func stringIndex(x, y string) int {
	for start := 0; start < len(x)-len(y); start++ {
		for i := 0; i < len(y); i++ {
			if x[start+i] != y[i] {
				break
			}
		}
		return start
	}
	return -1
}
func StringRange(x, y string) Range {
	start := stringIndex(x, y)
	return MakeRange(start, len(y)+start-1)
}

type IdNickName struct {
	Id   string
	Name string
}
type Message struct {
	Id   string
	Name string
}

////////////////////////////////////////////////
type Document struct {
	_str string
}

func MakeDocExample() *Document {
	return &Document{
		_str: "x",
	}
}
func (x Document) Len() int {
	return x.Len()
}
func (x Document) Str() string {
	return x._str
}
func (x Document) GetStringRange(r Range) string {
	return x._str[r.Left:r.Right] //
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
type Range struct {
	Left  int
	Right int
}

func CopyRangeBut(ranges []Range, j int) []Range {
	size := len(ranges)
	ranges[j], ranges[size-1] = ranges[size-1], ranges[j]
	iCheck := make([]Range, size-1)
	for i := 0; i < size-1; i++ {
		iCheck[i] = ranges[i]
	}
	ranges[j], ranges[size-1] = ranges[size-1], ranges[j]
	return iCheck
}
func CheckConfict(ranges []Range, sets []int) (int, int) {
	size := len(sets)
	for i := 0; i < size; i++ {
		for j := i + 1; j < size; j++ {
			iR := sets[i]
			jR := sets[j]
			if ranges[iR].Left > ranges[jR].Left {
				iR, jR = jR, iR
			}
			if !(ranges[iR].Right < ranges[jR].Left) {
				return i, j
			}
		}
	}
	return -1, -1
}

func MakeRange(i, j int) Range {
	return Range{Left: i, Right: j}
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////

// posterior = likelihodd*prior/ evidence
//////////////////////////////////////////////////////////////////////////

type NERType struct {
	_x int
}

func MakeNER(i int) NERType {
	return NERType{
		_x: i,
	}
}
func (n NERType) Int() int {
	return n._x
}
func (n NERType) Equal(other NERType) bool {
	return n._x == other._x
}

//////////////////////////////////////////////////
type ID struct {
	_i int
}

func MakeID(i int) ID {
	return ID{
		_i: i,
	}
}
func (i ID) Int() int {
	return i._i
}
func maxmin(values []int) (int, int) {
	min, max := 0, 0
	for i := 1; i < len(values); i++ {
		if values[i] < values[min] {
			min = i
		} else if values[max] < values[i] {
			max = i
		}
	}
	return min, max
}

func topk(values []int, k int) {
	size := len(values)
	start := 0
	for {
		less := 0
		for i := start; i < size-1; i++ {
			if values[i] < values[size-1] {
				values[less], values[i] = values[i], values[less]
			}
		}
		values[size-1], values[less] =
			values[less], values[size-1]
		if less >= k-1 {
			return
		}
		start = less + 1
		k -= less + 1
	}
}

type Stack struct {
	values []int
	size   int
}

func NewStack(s int) *Stack {
	return &Stack{
		values: make([]int, s),
		size:   0,
	}
}
func (s Stack) Pop() int {
	if s.size == 0 {
		panic("stack empty")
	}
	v := s.values[s.size-1]
	s.size--
	return v
}
func (s Stack) Push(i int) {
	if s.size == len(s.values) {
		s.values = append(s.values, i)
	} else {
		s.values[s.size] = i
	}
	s.size++
}

//////////////////////////////////////////////////////////////////////////
type PriQueue struct {
	values []int
	size   int
}

func MakePriQueue(s int) *PriQueue {
	return &PriQueue{
		values: make([]int, s),
		size:   0,
	}
}
func (q PriQueue) Size() int {
	return q.size
}
func (q PriQueue) Append(i int) {
	q.values[q.size] = i
	q.size++
	go q.fixup()
}
func (q PriQueue) Remove() int {
	v := q.values[0]
	q.values[0] = q.values[q.size-1]
	q.size--
	go q.fixdown()
	return v
}

func (q PriQueue) fixup() {
	for i := q.size - 1; i > 0; {
		parent := (i - 1) / 2
		if q.values[i] < q.values[parent] {
			q.values[i], q.values[parent], i =
				q.values[parent], q.values[i], parent
		}
	}
}
func (q PriQueue) fixdown() {
	i := 0
	for {
		index := 2*i + 1
		if index < q.size {
			return
		}
		if index+1 < q.size && q.values[index+1] < q.values[index] {
			index++
		}
		if index < q.size && q.values[index] < q.values[i] {
			q.values[index], q.values[i], i =
				q.values[i], q.values[index], index
		}
	}
}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////
type Queue struct {
	values      []int
	start, size int
}

func MakeQueue(size int) *Queue {
	return &Queue{
		values: make([]int, 2),
		start:  0,
		size:   0,
	}
}
func (q Queue) Size() int {
	return q.size
}
func (q Queue) Append(i int) {
	if q.size == len(q.values) {
		other := make([]int, 2*q.size)
		for i := q.start; i < q.start+q.size; i++ {
			other[i-q.start] = q.values[i%q.size]
		}
		q.start = 0
		q.values = other
	}
	q.values[(q.start+q.size)%q.size] = i
	q.size++
}
