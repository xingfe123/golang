package lbfgs

import "testing"
import "fmt"
import "github.com/gonum/matrix/mat64"

// f(x) = x_1^2 + x_2^2
func ff(w *mat64.Vector) float64 {
	return Product(w, w)
}
func f_1(w *mat64.Vector) *mat64.Vector {
	data := make([]float64, w.Len())
	for i := 0; i < w.Len(); i++ {
		data[i] = 2 * w.At(i, 0)
	}
	return mat64.NewVector(w.Len(), data)
}

func TestOptimizer(t *testing.T) {
	size := 500
	steps := 500
	optimizer := NewOptimizer(size, steps)
	w := mat64.NewVector(size, nil)
	f := make([]float64, 2)
	f[0] = 0.0
	f[1] = 0.0
	for i := 0; i < size; i++ {
		w.SetVec(i, 0.1)
	}
	k := 0
	for {
		fmt.Println("w = ", w)
		g := f_1(w)
		delta := optimizer.GetDeltaX(w, g)
		w.AddScaledVec(w, 1.0, delta)
		f[k%2] = ff(w)
		if f[0]-f[1] < 0.1 {
			fmt.Println("w = ", w)
			break
		}
		k++
	}

}
