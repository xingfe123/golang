package lbfgs

import (
	"fmt"
	"github.com/gonum/matrix/mat64"
	"log"
	"math"
)

////////////////////////////////////
/////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////
func Product(x, y *mat64.Vector) float64 {
	sum := 0.0
	for i := 0; i < x.Len(); i++ {
		sum += x.At(i, 0) * y.At(i, 0)
	}
	return sum
}
func NewZeroVector(n int) *mat64.Vector {
	return zeroVector(n)
}
func Norm(x *mat64.Vector) float64 {
	return math.Sqrt(Product(x, x))
}

//////////////////////////////////////////////////////
type Optimizer struct {
	m int
	k int
	x []*mat64.Vector
	g []*mat64.Vector
	////////////
	s                     []*mat64.Vector
	y                     []*mat64.Vector
	ro, z, q, alpha, beta *mat64.Vector
	dimension             int
}

func NewOptimizer(_dim, steps int) *Optimizer {
	_x := make([]*mat64.Vector, steps)
	_g := make([]*mat64.Vector, steps)
	_s := make([]*mat64.Vector, steps)
	_y := make([]*mat64.Vector, steps)
	for i := 0; i < steps; i++ {
		_x[i] = mat64.NewVector(_dim, nil)
		_y[i] = mat64.NewVector(_dim, nil)
		_g[i] = mat64.NewVector(_dim, nil)
		_s[i] = mat64.NewVector(_dim, nil)
	}
	return &Optimizer{dimension: _dim,
		k:     0,
		x:     _x,
		g:     _g,
		s:     _s,
		y:     _y,
		m:     steps,
		alpha: mat64.NewVector(steps, nil),
		beta:  mat64.NewVector(steps, nil),
		q:     mat64.NewVector(steps, nil),
		z:     mat64.NewVector(steps, nil),
		ro:    mat64.NewVector(steps, nil),
	}
}
func stationaryPoint(x *mat64.Vector) bool {
	x.ScaleVec(Norm(x), x)
	return x.At(0, 0) < 0.0001
}
func (opt *Optimizer) GetDeltaX(x, g *mat64.Vector) *mat64.Vector {
	//fmt.Println("start GetDeltax)")
	if x.Len() != opt.dimension || g.Len() != opt.dimension {
		log.Fatal("x或者g的维度和optimzier维度不一致")
	}

	currIndex := opt.k % opt.m

	// 更新x_k
	//fmt.Println("opt.x[currIndex].CloneVec(x)")
	opt.x[currIndex].CloneVec(x)

	// 更新g_k
	//fmt.Println("opt.g[currIndex].CloneVec(g)")
	opt.g[currIndex].CloneVec(g)

	// 当为第0步时，使用简单的带学习率的gradient descent
	if opt.k == 0 {
		vec := mat64.NewVector(opt.dimension, nil)
		for i := 0; i < opt.dimension; i++ {
			vec.SetVec(i, -g.At(i, 0)*0.0001)
		}
		opt.k++
		fmt.Println("end GetDeltax)")
		return vec
	}

	prevIndex := opt.k - 1%opt.m

	// 更新s_(k-1)
	opt.s[prevIndex].SubVec(opt.x[currIndex], opt.x[prevIndex])

	// 更新y_(k-1)
	opt.y[prevIndex].SubVec(opt.g[currIndex], opt.g[prevIndex])

	// 更新ro_(k-1)
	opt.ro.SetVec(prevIndex, 1.0/Product(opt.y[prevIndex], opt.s[prevIndex]))

	// 计算两个循环的下限
	lowerBound := opt.k - opt.m
	if lowerBound < 0 {
		lowerBound = 0
	}

	//	fmt.Println("第一个循环")
	opt.q.CloneVec(g)
	for i := opt.k - 1; i >= lowerBound; i-- {
		currIndex := i % opt.m
		opt.alpha.SetVec(currIndex,
			opt.ro.At(currIndex, 0)*Product(opt.s[currIndex], opt.q))
		opt.q.AddScaledVec(opt.q, -opt.alpha.At(currIndex, 0),
			opt.y[currIndex])
	}

	//fmt.Println("第二个循环")
	opt.z.CloneVec(opt.q)
	for i := lowerBound; i <= opt.k-1; i++ {
		currIndex := i % opt.m
		opt.beta.SetVec(currIndex,
			opt.ro.At(currIndex, 0)*Product(opt.y[currIndex], opt.z))
		opt.z.AddScaledVec(opt.z,
			opt.alpha.At(currIndex, 0)-opt.beta.At(currIndex, 0),
			opt.s[currIndex])
	}

	// 更新k
	opt.k++
	opt.z.ScaleVec(-1.0, opt.z)
	//fmt.Println("end GetDeltax)")
	return opt.z
}

func zeroVector(n int) *mat64.Vector {
	data := make([]float64, n)
	for i := 0; i < n; i++ {
		data[i] = 0.0
	}
	return mat64.NewVector(n, data)
}
