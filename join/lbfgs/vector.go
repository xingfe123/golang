package lbfgs

import (
	"github.com/gonum/matrix/mat64"
	"xingfe91/join/types"
)

type Mem struct {
	_mat *mat64.Dense
}

func (x Mem) Set(i int, y types.NERType, v float64) {
	if i <= 0 {
		panic("error")
	}
	x._mat.Set(i-1, y.Int(), v)
}
func (x Mem) Get(i int, y types.NERType) float64 {
	if i == 0 {
		return 1.0
	} else if i < 0 {
		return 0.0
	}
	return x._mat.At(i-1, y.Int())
}
func CreateMem(rows, cols int) *Mem {
	return &Mem{
		_mat: mat64.NewDense(rows, cols, nil),
	}
}

//////////////////////////////////////////////
func Mod(x, y int) int {
	if x < y {
		return x
	}
	return x - x/y*y
}
