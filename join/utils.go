package join

import (
	"fmt"
	"strings"
	"xingfe91/join/files"
	"xingfe91/join/types"
)

func GetGoldDocLabel() (map[string]*DocLabel, map[string]string) {
	goldWordLabel := make(map[string]*DocLabel)
	textTxt := make(map[string]string)
	var labels []*WordLabel
	prev := ""
	for line := range files.MakeLineChanFromPath(files.GoldFile) {
		data := strings.Split(line, "\t")
		if len(data) < 4 {
			continue
		}
		textTxt[data[0]] = data[1]
		now := data[0]
		if now == prev {
			labels = append(labels, &WordLabel{
				_range: types.StringRange(data[1], data[2]),
				_id:    data[3],
				_word:  data[2]})

		} else {
			if prev != "" {
				goldWordLabel[prev] = &DocLabel{
					doc:    textTxt[prev],
					labels: labels,
				}
			}
			labels = make([]*WordLabel, 1)
			labels[0] = &WordLabel{
				_range: types.StringRange(now, data[2]),
				_id:    data[3],
				_word:  data[2]}
			prev = now
		}

	}
	return goldWordLabel, textTxt
}
func PrintLnNotNil(err error) {
	if err != nil {
		fmt.Println(err)
	}
}

func ExtractTypes(dest string) {
	wordChan := make(chan string, 100)
	go writeTypesToChan(wordChan)
	files.WriteWordSetChan(dest, wordChan)
}
func writeTypesToChan(wordChan chan string) {
	graph := OpenGraph(files.GraphDir)
	golds, _ := GetGoldDocLabel()
	types := make(map[string]bool)
	for _, docL := range golds {
		//fmt.Println(docL.doc)
		//fmt.Println(docL.Len())
		for _, x := range docL.labels {
			for ner := range graph.Types(x._id) {
				if types[ner] == false {
					wordChan <- ner
					types[ner] = true
					fmt.Println(ner)
				}
			}
		}
	}
	close(wordChan)
}
