package publish

import (
	"errors"
	"fmt"
	"sync"
	"time"
)

type Message string

var (
	ErrTopicClosed = errors.New("Topic has been closed")
)

///////////////////
// singleton
type Object struct {
}

var (
	once     sync.Once
	instance *Object
)

func getInstance() *Object {
	once.Do(func() {
		instance = &Object{}
	})
	return instance
}

/////////////////////////
type Topic struct {
	subscribers    []Authentication
	MessageHistory []struct {
		Author    string
		Message   Message
		Timestamp time.Time
	}
}
type Subscription struct {
	ch    chan<- Message
	Inbox chan Message
}

func (s Subscription) Publish(msg Message) error {
	return nil
}

type Authentication interface {
}

////////////////////////////////
// Lazy Eva
type LazyInt chan func() int

func (l LazyInt) Future(i int) {
	l <- func() int {
		time.Sleep(3 * time.Second)
		return i
	}
}
func (l LazyInt) Add(i int) {
	old := <-l
	l <- func() int {
		return old() + i
	}
}

////////////////////////////////////
// parallel For loop

func parallel() {
	N := 10
	data := make([]float64, N)
	sem := make(chan bool, N)
	dosomething := func(i int, xi float64) {
		// dosomething
		sem <- true
	}
	loop := func() {
		for i, xi := range data {
			go dosomething(i, xi)
		}
		close(sem)
	}
	go loop()
	for i := 0; i < N; i++ {
		<-sem
	}

}

///////////////////////////////
// producer consumer
func others() {
	jobs := make(chan int, 5)
	done := make(chan bool)
	dosomething := func() {
		for j := range jobs {
			fmt.Println("received job", j)
		}
		fmt.Println("received all jobs")
		done <- true
	}
	for j := 0; j < 3; j++ {
		jobs <- j
	}
	close(jobs)
	<-done
}
func producer_consumer() {
	done := make(chan bool)
	N := 10
	msgs := make(chan int, N)
	produce := func() {
		for i := 0; i < N; i++ {
			msgs <- i
		}
		close(msgs)
	}
	consume := func() {
		for msg := range msgs {
			fmt.Println(msg)
		}
		done <- true
	}
	go produce()
	consume()
	<-done
}

//////////////////
// iterator
func iter() <-chan int {
	ch := make(chan int)
	loop := func() {
		for i := 0; i < 10; i++ {
			ch <- i
		}
		close(ch)
	}
	go loop()
	return ch
}
