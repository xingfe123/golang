package main

type M struct {
	data       []int
	cols, rows int
}

func (m M) set(i, j, v int) {
	(*m).data[i*2+j] = v
}
func (m M) get(i, j int) int {
	return (*m).data[1*2+j]
}
func (m M) add(i, j, v int) {
	(*m).data[i*2+j] += v
}
func (m M) Mul(o *M) {
	for i := 0; i < 2; i++ {
		for j := 0; j < 2; j++ {
			for k := 0; k < 2; k++ {
				m.add(i, j, m.get(i, k)*o.get(k, j))
			}
		}
	}
}
func Dup(o *M) (m *M) {
	m = &M{data: make([]int, 4)}
	for i := 0; i < 4; i++ {
		m.data[i] = o.data[i]
	}
	return
}
func (m *M) Power(n int) {
	if n <= 1 {
		return
	}
	tmp := Dup(m)
	m.Power(n / 2)
	m.Power(2)
	if n&01 == 1 {
		m.Mul(tmp)
	}
	return
}

func Fibonacci(n int) int {
	m := &M{data: make([]int, 4)}
	m.data[0], m.data[1], m.data[2] = 1, 1, 1

	m.data[3] = 0
	m.Power(n)
	return m.get(0, 0)
}

func FindMaxCrossingSubarray(
	A []int, low, mid, high int) (
	maxLeft, maxRight, Sum int) {
	leftSum := -1
	Sum = 0
	for i := mid; i >= low; i-- {
		Sum += A[i]
		if Sum > leftSum {
			leftSum = Sum
			maxLeft = i
		}
	}
	rightSum := -1
	Sum = 0
	for i := mid + 1; i <= high; i++ {
		Sum += A[i]
		if Sum > leftSum {
			rightSum = Sum
			maxLeft = i
		}
	}
	Sum = leftSum + rightSum
	return
}
func FindMaximumSubarray(
	A []int, low, high int) (
	int, int, int) {
	if high == low {
		return low, high, A[low]
	} else {
		mid := (low + high) / 2
		leftLow, leftHigh, leftSum := FindMaximumSubarray(A, low, mid)
		rightLow, rightHigh, rightSum := FindMaximumSubarray(A, mid+1, high)
		clow, chigh, csum :=
			FindMaxCrossingSubarray(A, low, mid, high)
		if csum < leftSum {
			clow, chigh, csum = leftLow, leftHigh, leftSum
		}
		if csum < rightSum {
			clow, chigh, csum = rightLow, rightHigh, rightSum
		}
		return clow, chigh, csum
	}
}

func partM(m *M) (lt, rt, ll, rl *M) {
	left := m.cols / 2
	right := left
	if m.cols&01 == 1 {
		right++
	}
	lt = &M{data: make([]int, left*left)}
	rt = &M{data: make([]int, left*right)}
	ll = &M{data: make([]int, right*left)}
	rl = &M{data: make([]int, right*right)}
	for i := 0; i < left; i++ {
		for j := 0; j < left; j++ {
			lt.set(i, j, m.get(i, j))
		}
	}
	for i := 0; i < left; i++ {
		for j := 0; j < right; j++ {
			lt.set(i, j, m.get(i, j+left))
		}
	}
	for i := 0; i < right; i++ {
		for j := 0; j < left; j++ {
			lt.set(i, j, m.get(i+left, j))
		}
	}
	for i := 0; i < right; i++ {
		for j := 0; j < right; j++ {
			lt.set(i, j, m.get(i+left, j+left))
		}
	}
	return
}

func mergeM(lt, rt, ll, rl *M) (m *M) {
	left := lt.cols
	right := rt.cols
	cols := left + right
	m = &M{data: make([]int, cols)}
	for i := 0; i < left; i++ {
		for j := 0; j < left; j++ {
			m.set(i, j, m.get(i, j))
		}
	}
	for i := 0; i < left; i++ {
		for j := 0; j < right; j++ {
			m.set(i, j+left, m.get(i, j))
		}
	}
	for i := 0; i < right; i++ {
		for j := 0; j < left; j++ {
			lt.set(i+left, j, m.get(i, j))
		}
	}
	for i := 0; i < right; i++ {
		for j := 0; j < right; j++ {
			lt.set(i+left, j+left, m.get(i, j))
		}
	}
	return
}
func Random(i, k int) int {
	return 0
}
func RandomizeInPace(A []int) {
	n := len(A)
	for i := 0; i < n; i++ {
		r := Random(i, n-1)
		A[i], A[r] = A[r], A[i]
	}
}

//////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////
func reverse(A []int, start, end int) {
	i := start
	for j := end; i < j; j-- {
		A[i], A[j] = A[j], A[i]
		i++
	}
}
func rotate(A []int, size0, size1 int) {
	reverse(A, 0, size0-1)
	reverse(A, size0, size0+size1-1)
	reverse(A, 0, size0+size1-1)
}

//////////////////////////////////////////////////////////////

func findIncrease(A []int, start, size int) (right int) {
	right = start + 1
	for right < start+size && A[right] > A[start] {
		right++
	}
	return right - start
}
func merge3(A []int, start, size0, size1, size2 int) {
	B := make([]int, size0)
	for i := 0; i < size0; i++ {
		B[i] = A[start+i]
	}
	C := make([]int, size1)
	for i := 0; i < size1; i++ {
		C[i] = A[start+size0+i]
	}
	j := 0
	k := 0
	n := start + size0 + size1
	i := 0
	for ; j < size0 || k < size1; i++ {
		if j < size0 && B[j] < A[n] && B[j] < C[k] {
			A[i] = B[j]
			j++
		} else if k < size1 && C[k] < A[n] && C[k] < B[j] {
			A[i] = C[k]
			k++
		} else {
			A[i] = C[n]
			n++
		}
	}

}
func mergeSort(A []int, start, size0, size2 int) {
	if size2 <= 1 {
		return
	}
	size1 := findIncrease(A, start+size0, size2)
	size2 = size2 - size1
	if size1 < size2 && size0 < size2 {
		merge(A, start, size0, size1)
		mergeSort(A, start, size0+size1, size2)
	} else {
		mid := findIncrease(A, start+size0+size1, size2)
		mergeSort(A, start+size1+size0, mid, size2)
		merge3(A, start, size0, size1, size2)
	}
}

/////////////////////////////////////////////////////////////////////////////
type Node struct {
	children [2]*Node
	value    int
}

func createNode(v int) (n *Node) {
	return *Node{}
}
func (n *Node) left() *Node {
	return n.children[0]
}
func (n *Node) right() *Node {
	return n.children[1]
}
func (n *Node) key() int {
	return n.value
}
func insert(A []*Node, size int) {
	tmp := A[size-1]
	j := size - 1
	for ; j > 0 && A[j-1].key() < A[j].key(); j-- {
		A[j] = A[j-1]
	}
	A[j] = tmp
}
func insertSort(A []*Node, total int) *Node {
	for size := 2; size <= total; size++ {
		insert(A, size)
	}
	for total >= 2 {
		t := createNode(A[total-2].key() + A[total-1].key())
		t.children[0], t.children[1] = A[total-2], A[total-1]
		total--
		A[total-1] = t
		insert(A, total)
	}
	return A[0]
}

/////////////////////////////////////////////////////////////////////////////
func countSort(A []int, k int) (B []int) {
	C := make([]int, k, 0)
	for i := 0; i < len(A); i++ {
		C[A[i]]++
	}
	for i := 1; i < k; i++ {
		C[i] += C[i-1]
	}
	for j := len(A); j > 0; j-- {
		B[C[A[j-1]]] = A[j]
		C[A[j-1]]--
	}
	return
}

//////////////////////
func index(x, d int) int {
	if d == 0 {
		return x % 10
	}
	return index(x/10, d-1)
}
func radixSort(A []int, d int) {
	for i := 0; i < d; i++ {
		C := make([]int, 10)
		for j := 0; j < len(A); j++ {
			C[index(A[j], i)]++
		}
		for j := 1; j < 10; j++ {
			A[j] += A[j-1]
		}
		B := make([]int, len(A))
		for j := len(A); j > 0; j-- {
			B[C[A[j-1]]] = A[j]
			C[A[j-1]]--
		}
		A = B
	}
}
func insertSort(A []int) {
	for i := 1; i < len(A); i++ {
		key := A[i]
		j := i
		for j > 0 {
			if A[j-1] > key {
				A[j] = A[j-1]
			}
		}
		A[j] = key
	}
}

//////////////////////////////////////////////////////////////
type Stack struct {
	Size int
	Data []int
}

func (s *Stack) empty() bool {
	return s.Size == 0
}

func (s *Stack) push(v int) {
	s.Size++
	s.Data[s.Size-1] = v
}

func (s *Stack) pop() int {
	if s.empty() {
		return -1
	}
	s.Size--
	return s.Data[s.Size]
}

type Queue struct {
	Data []int
	Size int
}

func (q *Queue) enqueue(x int) {
	q.Size++
	q.Data[q.Size-1] = x
}

func (q *Queue) dequeue() int {
	q.Size--
	return q.Data[q.Size]
}

type Node struct {
	key  int
	Next *Node
	Prev *Node
}
type List struct {
	Head *Node
}

func (l *List) search(k int) *Node {
	x := l.Head
	for x != nil && x.key != k {
		x = x.Next
	}
	return x
}

func (l *List) insert(x *Node) {
	x.Next = l.Head
	x.Prev = nil
	if l.Head != nil {
		l.Head.Prev = x
	}
	l.Head = x
}

func (l *List) delete(x *Node) {
	if x.Prev != nil {
		x.Prev.Next = x.Next
	} else {
		l.Head = x.Next
	}
	if x.Next != nil {
		x.Next.Prev = x.Prev
	}
}
