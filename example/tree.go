package tree
type HashTable struct{
	Data []int
}
/////////////////////////////////////////////////////////////
type Tree struct{
	
}
type Node struct{
	Left, Right, Parent *Node
	Key int
}
func walk(x *Node){
	walk(x.Left)
	print(x.Key)
	walk(x.Right)
}
func search(x *Node, k int)(*Node){
	if x==nil || k == x.Key{
		return x
	}
	if k < x.Key{
		return search(x.Left, k)
	}
	return search(x.Right, k)
}

func Minimum(x *Node)(*Node){
	for x.Left != nil{
		x = x.Left
	}
	return x
}

func Maximum(x *Node)(*Node){
	for x.Right != nil{
		x = x.Right
	}
	return x
}

func Successor(x *Node)(*Node){
	if x.Right != nil{
		return Minimum(x.Right)
	}
	y := x.Parent
	for y !=nil && x == y.Right{
		x = y
		y = y.Parent
	}
	return y
}

func Insert(x, z *Node){
	var y *Node
	for x != nil{
		y =x
		if z.Key < x.Key{
			x = x.Left
		}else{
			x = x.Right
		}
	}
	z.Parent = y
	if y == nil{
		x = z
	}else if z.key < y.key{
		y.Left = z
	}else{
		y.Right = z
	}
}











func  key(x int) int{
	return x
}
func hash(k int, i int) int{
	return k+i
}
func (h *HashTable) addressInsert(x int){
	h.Data[key(x)] = x
}
func (h *HashTable) addressDelete(x int){
	h.Data[key(x)] = x
}

func (h *HashTable) Search(k int)(int){
	i := 0
	for{
		j := hash(k, i)
		if h.Data[j] == k{
			return j
		}
		i++
		if h.Data[j] == -1{
			return 0
		}
	}
}
