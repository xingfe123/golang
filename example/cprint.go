package main

/*
#include<stdio.h>
*/



type M struct{
	data []int
	cols, rows int
}
func (m *M) set( i, j , v int){
	(*m).data[i*2+j] = v
}
func (m *M) get( i, j int) int{
	return (*m).data[1*2+j]
}
func (m *M) add(i, j, v int){
	(*m).data[i*2+j] += v
}
func (m *M) Mul(o *M){
	for i:=0; i<2; i++{
		for j:=0 ; j<2; j++{
			for k:=0; k<2 ;k++{
				m.add(i,j, m.get(i,k) * o.get(k,j))
			}
		}
	}
}
func Dup(o *M)(m* M){
	m = &M{data: make([]int, 4)}
	for i:=0 ;i<4; i++{
		m.data[i] = o.data[i]
	}
	return 
}
func (m *M) Power(n int){
	if n<= 1 {
		return
	}
	tmp := Dup(m)
	m.Power(n/2)
	m.Power(2)
	if n & 01 ==1{
		m.Mul(tmp)
	}
	return 
}

func Fibonacci(n int) int {
	m := &M{data: make([]int, 4)}
	m.data[0], m.data[1], m.data[2]=1,1,1
	
	m.data[3] = 0
	m.Power(n)
	return m.get(0,0)
}


func FindMaxCrossingSubarray(
	A []int, low, mid, high int)(
	maxLeft, maxRight, Sum int){
	leftSum := -1
	Sum = 0
	for i := mid; i>=low; i--{
		Sum += A[i]
		if Sum > leftSum{
			leftSum = Sum
			maxLeft = i
		}
	}
	rightSum := -1
	Sum =0
	for i := mid+1; i<=high; i++{
		Sum += A[i]
		if Sum > leftSum{
			rightSum = Sum
			maxLeft = i
		}
	}
	Sum = leftSum + rightSum
	return 
}
func FindMaximumSubarray(
	A []int, low, high int)(
		int , int ,int){
	if high == low{
		return low , high, A[low]
	}else{
		mid := (low+high)/2
		leftLow, leftHigh, leftSum := FindMaximumSubarray(A, low , mid)
		rightLow, rightHigh, rightSum := FindMaximumSubarray(A, mid+1, high)
		clow, chigh, csum :=
			FindMaxCrossingSubarray(A, low, mid, high)
		if csum < leftSum{
			clow, chigh, csum = leftLow, leftHigh, leftSum
		}
		if csum < rightSum{
			clow, chigh, csum = rightLow, rightHigh, rightSum
		}
		return clow, chigh, csum
	}
}

func partM(m *M)(lt,rt, ll, rl *M){
	left := m.cols/2
	right := left
	if m.cols &01==1{
		right++
	}
	lt = &M{data: make([]int, left*left)}
	rt = &M{data: make([]int, left*right)}
	ll = &M{data: make([]int, right*left)}
	rl = &M{data: make([]int, right*right)}
	for i:=0; i<left; i++{
		for j:=0; j<left; j++{
			lt.set(i, j, m.get(i, j))
		}
	}
	for i:=0; i<left; i++{
		for j:=0; j<right; j++{
			lt.set(i, j, m.get(i, j+left))
		}
	}
	for i:=0; i<right; i++{
		for j:=0; j<left; j++{
			lt.set(i, j, m.get(i+left, j))
		}
	}
	for i:=0; i<right; i++{
		for j:=0; j<right; j++{
			lt.set(i, j, m.get(i+left, j+left))
		}
	}
	return 
}

func mergeM(lt, rt, ll, rl *M)(m *M){
	left := lt.cols
	right := rt.cols
	cols := left+right
	m = &M{data : make([]int, cols)}
	for i:=0; i<left; i++{
		for j:=0; j<left; j++{
			m.set(i, j, m.get(i, j))
		}
	}
	for i:=0; i<left; i++{
		for j:=0; j<right; j++{
			m.set(i, j+left, m.get(i, j))
		}
	}
	for i:=0; i<right; i++{
		for j:=0; j<left; j++{
			lt.set(i+left, j, m.get(i, j))
		}
	}
	for i:=0; i<right; i++{
		for j:=0; j<right; j++{
			lt.set(i+left, j+left, m.get(i, j))
		}
	}
	return 
}
func Random(i, k int) int{
	return 0
}
func RandomizeInPace(A []int){
	n := len(A)
	for i :=0; i<n; i++{
		r := Random(i,n-1)
		A[i], A[r] = A[r], A[i]
	}
}
////////////////////////////////////////////////////

func parent(i int)int{
	return (i-1)/2
}
type Heap struct{
	data []int
	size int
}
func (A *Heap) heapSize()int{
	return A.size
}
func (A *Heap) get(i int) int{
	return A.data[i]
}
func left(i int) int{
	return i*2 +1
}
func right(i int) int{
	return i*2+2
}
func maxHeapify(A []int, size, i int){
	l := left(i)
	r := right(i)
	largest := i
	if l < size && A[l] >A[largest]{
		largest = l
	}
	if r < size && A[r] > A[largest]{
		largest = r
	}
	if largest != i{
		A[i], A[largest] = A[largest], A[i]
		maxHeapify(A, size, largest)
	}
}
func buildMaxHeapify(A []int){
	size := len(A)
	for i :=size/2; i>=0; i--{
		maxHeapify(A, size, i)
	}
}

func heapSort(A []int){
	buildMaxHeapify(A)
	for i := len(A); i>0; i--{
		A[0], A[i-1] = A[i-1], A[0]
		maxHeapify(A, i-1,0)
	}
}

func heapMax(A []int) int{
	return A[0]
}
func heapExtractMax(A []int, size *int) int{
	if *size < 1{
		return 0
	}
	max := A[0]
	A[0] = A[*size-1]
	*size--
	maxHeapify(A, *size, 0)
	return max
}

func heapIncreaseKey(A []int, i int, key int){
	A[i] = key
	for i>0 && A[parent(i)] < A[i]{
		A[parent(i)], A[i] = A[i], A[parent(i)]
		i = parent(i)
	}
}

func maxHeapInsert(A []int, size *int, key int){
	*size++
	heapIncreaseKey(A, *size, key)
}
////////////////////////////////////
func quickSort(A []int, p, r int){
	for p >=r {
		q := partition(A, p , r)
		quickSort(A, p, q-1)
		p = q+1
	}
}
func partition(A []int, p, r int) int{
	x := A[r]
	i := p
	for j :=p; j<r; j++{
		if A[j] <= x{
			A[i], A[j] = A[j], A[i]
			i++
		}
	}
	A[i], A[r] = A[r], A[i]
	return i
}
//////////////////////////
func countSort(A []int,k int)(B []int){
	C := make([]int, k, 0)
	for i :=0; i< len(A); i++{
		C[A[i]]++
	}
	for i :=1; i< k; i++{
		C[i] += C[i-1]
	}    
	for j := len(A); j>0; j--{
		B[C[A[j-1]]] = A[j]
		C[A[j-1]]--
	}
	return
}
//////////////////////
func index( x ,  d int) int{
	if d== 0{
		return x%10
	}
	return index(x/10, d-1)
}
func radixSort(A []int, d int){
	for i:=0; i<d ;i++{
		C := make([]int, 10)
		for j :=0; j<len(A); j++{
			C[index(A[j], i)]++
		}
		for j:=1; j<10; j++{
			A[j] += A[j-1]
		}
		B := make([]int, len(A))
		for j :=len(A); j>0;j--{
			B[C[A[j-1]]] = A[j]
			C[A[j-1]]--
		}
		A = B
	}
}
func insertSort(A []int){
	for i:=1; i< len(A); i++{
		key := A[i]
		j := i
		for j>0 {
			if A[j-1] > key{
				A[j] = A[j-1]
			}
		}
		A[j] = key
	}
}
//////////////////////////////////////////////////////////////
type Stack struct{
	Size int
	Data []int
}

func (s *Stack) empty() bool{
	return s.Size == 0
}

func (s *Stack) push(v int){
	s.Size++
	s.Data[s.Size-1] = v
}

func (s *Stack) pop() int{
	if s.empty(){
		return -1
	}
	s.Size--
	return s.Data[s.Size]
}

type Queue struct{
	Data []int
	Size int
	
}

func (q *Queue) enqueue( x int){
	q.Size++
	q.Data[q.Size-1] = x
}

func (q *Queue) dequeue() int{
	q.Size--
	return q.Data[q.Size] 
}
type Node struct{
	key int
	Next *Node
	Prev *Node
}
type List struct{
	Head *Node	
}

func (l *List) search( k int)(*Node){
	x := l.Head
	for x != nil && x.key !=k {
		x = x.Next
	}
	return x
}

func (l *List) insert(x *Node){
	x.Next = l.Head
	x.Prev = nil
	if l.Head != nil{
		l.Head.Prev = x
	}
	l.Head = x
}

func (l* List) delete(x *Node){
	if x.Prev != nil{
		x.Prev.Next = x.Next
	}else{
		l.Head = x.Next
	}
	if x.Next != nil{
		x.Next.Prev = x.Prev
	}
}

