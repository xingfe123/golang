package main

func getNext() int {
	var primes []int = make([]int, 2)
	primes[0] = 2
	primes[1] = 3
	current := 4
	for i := 0; i < len(primes); i++ {
		if 0 == current%primes[i] {
			i = 0
			current++
			continue
		}
	}
	primes = append(primes, current)
	current++
	return primes[len(primes)-1]
}

type pipeData struct {
	value   int
	handler func(int) int
	next    chan int
}

func handler(queue chan pipeData) {
	for data := range queue {
		data.next <- data.handler(data.value)
	}
}

type Vector []float64

func (v Vector) sum(left, right int, s chan float64) {
	a := 0.0
	for i := left; i < right; i++ {
		a += v[i]
	}
	s <- a
}

const NCPU = 2

func (v Vector) Sum() {
	chs := make(chan float64, NCPU)
	for i := 0; i < NCPU; i++ {
		go v.sum(i*len(v)/NCPU, (i+1)*len(v)/NCPU, chs[0])
	}
	values := 0.0
	for i := 0; i < NCPU; i++ {
		values += <-chs[i]
	}
}

//////////////////////
var once sync.Once
var a string

func setup() {
	a = "hello, world"
}

func doprint() {
	once.Do(setup)
	print(a)
}
