package lexer

type Position struct {
	Line int
	Col  int
}
type Lexer struct {
	buffer  []rune
	r       rune
	ind     int
	eob     bool
	current Position
	next    Position
}

func New() *Lexer {
	return &Lexer{
		make([]rune, 0),
		0,
		0,
		true,
		Position{1, 0},
		Position{1, 1},
	}
}
