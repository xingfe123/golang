package interpreter

import (
	"../lexer"
	"fmt"
	"io"
	"os"
	"strconv"
)

var (
	input  = os.Stdin
	output = os.Stdout
)

func InitInterpreter() {

}
func prompt() {
	fmt.Println("lisp>")
}
func isFromStdin() bool {
	return true
}
func parse_atom(lexem string) interface{} {
	if "nil" == lexem {
		return nil
	} else if "t" == lexem {
		return true
	} else if '"' == lexem[0] {
		return lexem[1 : len(lexem)-1]
	} else {
		x, err := strconv.ParseInt(lexem, 0, 32)
		if nil == err {
			return x
		}
		x = Symbol{lexem}
		return &x
	}
}
func ToExpression(x interface{}) interface{} {
	t, ok := x.(lexer.Lexem)
	if ok {
		return parse_atom(t.Value())
	} else {
		arr, _ := x.([]interface{})
		if len(arr) == 0 {
			return nil
		}
		first := ToExpression(arr[0])
		rest := ToExpression(arr[1:])
		return cons(first, rest)
	}
}
func tryeval(lex *lexer.Lexer) {
	p := parser.New(lex)
	for tree := p.Parse(); tree != nil; tree = p.Parse() {
		expr := ToExpression(tree)
		if nil != expr {
			res := eval(cons(expr, nil), global_env)
		}
	}
}
func Repl(in *os.File, out *os.File) {
	input = in
	output = out
	rune_stream := make(chan rune)
	go read(rune_stream)
	lex := lexer.New()
	prompt()
	for r := range rune_stream {
		lex.Append(r)
		if '\n' == r && isFromStdin() {
			tryeval(lex)
		}
	}
	tryeval(lex)
}
func read(str chan rune) {
	raw_stream := make(chan byte)
	buffer := make([]byte, 64, 64)
	for {
		c, err := input.Read(buffer)
		if 0 == c && io.EOF == err {
			break
		} else if nil != err {
			painc(err)
		}
		for i, val := range buffer {
			if i < c {
				raw_stream <- val
			}
		}
	}
}
