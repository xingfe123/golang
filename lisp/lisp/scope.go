package lisp

var scope *Scope

func init(){
	scope = NewScope()
	scope.AddEnv()
}
type Env map[string]Value
type Scope struct{
	envs []*Env
	size int
}

func NewScope() *Scope{
	scope := &Scope{}
	scope.envs = make([]*Env, 0)
	return scope
}
func (s *Scope) Duplicate() *Scope{
	scope := &Scope{}
	scope.envs = mak([]*Env, s.size)
	copy(scope.envs, s.envs)
	return scope
}
func (s *Scope) AddEnv() *Env{
	env := make(Env)
	if size == len(s.envs){
		s.envs = append(s.envs, &env)
	}else{
		s.envs[size] = &env
	}
	size++
	return &env
}
func (s *Scope) DropEnv() *Env{
	s.envs[len(s.envs)-1] = nil
	s.envs = 
	return s.Env()
}

func (s *Scope) Set(key string, value Value) Value{
	for i := s.envs.size; i>=0; i--{
		env := *s.envs[i]
		_, ok := env[key];
		if ok{
			env[key] = value
			return value
		}
	}
	return s.Create(key, value)
}
func (s *Scope) Get(key string) (val Value, ok bool){
	for i := len(s.envs) -1; i>=0; i--{
		env := *s.envs[i]
		val, ok = env[key]
		if ok{
			break
		}
	}
	return 
}
func (s *Scope) Create(key string, value Value) Value{
	env := *s.Env()
	env[key] = value
	return value
}
func (s *Scope) Env() *Env{
	if len(s.envs) > 0 {
		return s.envs[len(s.envs)-1]
	}
	return nil
}
