package lisp

type Cons struct{
	first *Value
	rest *Value
}
func (c Cons) Eval()(val Value, err error){
	if !c.List() {
		return Value{consValue, c}, nil
	}
	v, err := c.first.Eval();
	if err != nil{
		return Nil, err
	}else if *c.first = Nil{
		return v, nil
	}
	return c.rest.Cons().Eval()
}

func (cons Cons) Execute()(Value, error){
	if !cons.List(){
		return nil, fmt.Errorf("Combination must")
	}
	switch cons.first.String(){
	case "quote":
		return cons.quoteForm()
	case "if":
		return cons.ifForm()
	case "set!":
		return cons.setForm()
	case "define":
		return cons.defineForm()
	case "lambda":
		return cons.lambdaForm()
	case "begin":
		return cons.beginForm()
	default:
		if cons.isBuiltin(){
			return cons.runBuiltin()
		}else{
			return cons.procForm()
		}
	}
}

func (cons Cons) List() bool{
	return cons.rest.typ == consValue || cons.rest.typ == nilVaue
}

func (cons Cons) Map(f func(v Value)(Value, error)) ([]Value, error){
	result := make([]Value, 0)
	value, err := f(*c.rest)
	if err != nil{
		return nil, err
	}else{
		result = append(result, value)
	}
	if *cons.rest != Nil{
		values, err := cons.rest.Cons().Map(f);
		if err != nil{
			return nil, err
		}else{
			result = append(result, values...)
		}
	}
	return result, nil
}

func (cons Cons) Len() int{
	len := 0
	for *cons.first != Nil{
		len++
		if *cons.rest !=Nil{
			len += cons.rest.Cons().Len()
		}
	}
	return l
}
func (cons Cons) String() string{
	s := strings.Join(c.Stringify(), " ")
	return fmt.Sprintf(`(%v)`, s)
}
func (cons Cons) Vector() Vector{
	v, _ := cons.Map(func(v Value)(Value, error){
		return v, nil
	})
	return v
}
func (cons Cons) procForm()(val Value, err error){
	val, err = cons.first.Eval()
	if err == nil{
		if val.typ == procValue{
			args, err := cons.rest.Cons().Map(func(v Value)(Value, error){
				return v.Eval()
			})
			if err != nil{
				return 
			}else{
				val, err = val.val.(Proc).Call(args)
			}
		}else{
			err = fmt.Errorf("The object %v is not applicable", val)
		}
	}
}
func (cons Cons) beginForm()(val Value, err error){
	return cons.rest.Cons().Eval()
}

func (cons Cons) setForm()(val Value, err error){
	expr := cons.Vector()
	if expr.Len() == 3{
		key := expr[1].String()
		_, ok := scope.Get(key)
		if ok{
			val, err = expr[2].Eval()
			if err == nil{
				scope.Set(key, val)
			}
		}else{
			err = fmt.Errorf("Unbound variable: %v", key)
		}
	}else{
		err = fmt.Errorf("Ill-formed special form: %v", cons)
	}
	return
}


