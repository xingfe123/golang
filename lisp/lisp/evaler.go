package lisp

func EvalString(line string)(Value, error){
	expand, err := NewTokens(line).Expand()
	if err != nil{
		return Nil, err
	}
	parsed, err := expand.Parse()
	if err != nil{
		return Nil, err
	}
	evaled, err := parsed.Eval()
	if err != nil{
		return Nil, err
	}
	scope.Create("_", evaled)
	return evaled, nil
}
