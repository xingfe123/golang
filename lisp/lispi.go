package main

import (
	lispi "./interpreter"
	"fmt"
)

func init_repl() {
	fmt.Println("++++++++++++++++++")
	lispi.InitInterpreter()
	lispi.Repl(in, out)
	fmt.Println("------------------")
}

func main() {
	init_repl()
}
