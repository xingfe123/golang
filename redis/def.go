package redis

type char byte
type sdshdr struct {
	buf  []char
	len  int
	free int
}

func newsds(s []char) *sdshdr {
	size := 2 * len(s)
	buf := make([]char, size)
	for i := 0; i < size/2; i++ {
		buf[i] = s[i]
	}
	return &sdshdr{
		buf:  buf,
		len:  size / 2,
		free: size / 2,
	}
}
func emptysds() *sdshdr {
	return &sdshdr{
		buf:  make([]char, 4),
		len:  0,
		free: 4,
	}
}
func freesds(x *sdshdr) {
}
func lensds(x *sdshdr) int {
	return x.len
}
func vailsds(x *sdshdr) int {
	return x.free
}
func dupsds(x *sdshdr) *sdshdr {
	y := &sdshdr{
		buf:  make([]char, x.free+x.len),
		len:  x.len,
		free: x.free,
	}
	for i := 0; i < x.len; i++ {
		y.buf[i] = x.buf[i]
	}
	return y
}

func clearsds(x *sdshdr) {
	x.len, x.free = 0, x.len
}

func cartsds(x, y *sdshdr) {
	if x.free < y.len {
		_expandsds(x, x.len+y.len)
	}
	for i := 0; i < y.len; i++ {
		x.buf[x.len+i] = y.buf[i]
	}
	x.len += y.len
	x.free -= y.len
}

func cmpsds(x, y *sdshdr) int {
	for i := 0; ; i++ {
		if i == x.len && i == y.len {
			return 0
		} else if i == x.len {
			return -1
		} else if i == y.len {
			return 1
		} else if c := x.buf[i] - y.buf[i]; c != 0 {
			return int(c)
		}
	}
}

func _expandsds(x *sdshdr, size int) {
	if size > x.len {
		buf := make([]char, size+size)
		for i := 0; i < x.len; i++ {
			buf[i] = x.buf[i]
		}
		x.buf, x.free = buf, 2*size-x.len
	}
}

///////////////////////////////////////////////////////
type ListNode struct {
	prev, next *ListNode
	value      *interface{}
}

type list struct {
	head, tail *ListNode
	len        uint
	dup        func(ptr *ListNode) *ListNode
	free       func(ptr *ListNode)
	match      func(ptr *ListNode, key *interface{}) int
}

func listLength(x *list) int {
	return int(x.len)
}
func listFirst(x *list) *ListNode {
	return x.head
}
func listLast(x *list) *ListNode {
	return x.tail
}
func listNextNode(n *ListNode) *ListNode {
	return n.next
}
func listPrevNode(n *ListNode) *ListNode {
	return n.prev
}
func listNodeValue(n *ListNode) *interface{} {
	return n.value
}
func listSearchKey(x *list, key *interface{}) *ListNode {
	for z := x.head; z != x.tail.next; z = z.next {
		if c := x.match(z, key); c == 0 {
			return z
		}
	}
	return nil
}
func listDup(x *list) *list {
	index := func(j int) int {
		if j == int(x.len) {
			return 0
		}
		return j
	}
	nodes := make([]*ListNode, x.len)

	for i, z := 0, x.head; i < int(x.len); i, z = i+1, z.next {
		nodes[i] = x.dup(z)
		nodes[i].next = nodes[index(i+1)]
		nodes[index(i+1)].prev = nodes[i]
	}
	y := &list{
		head:  nodes[0],
		tail:  nodes[x.len-1],
		dup:   x.dup,
		free:  x.free,
		match: x.match,
		len:   x.len,
	}

	return y
}

///////////////////////////////////////////////////////////
type robj struct {
}
type zskiplistLevel struct {
	forward *zskiplistNode
	span    uint
}

type zskiplistNode struct {
	level    []zskiplistLevel
	backward *zskiplistNode
	score    float64
	obj      *robj
}
type zskiplist struct {
	head, tail *zskiplistNode
	length     uint
	level      int
}

func zslCreate() *zskiplist {
	return nil
}
func zslInsert(x *zskiplist, v *interface{}) {

}

////////////////////////////////////////////////////
func ziplistNew() {
}
func ziplistPush() {
}
func ziplistInsert() {
}
func ziplistLen() {
}

//////////////////////////////////////////////////
type intset struct {
	encoding uint32
	length   uint32
	contents []int8
}

func intsetNew() *intset {
	return &intset{
		encoding: 1,
		length:   0,
		contents: make([]int8, 0),
	}
}
func intsetAdd(x *intset, i int) {
	//todo
}
func intsetRemove(x *intset, i int) {
	//todo
}
func intsetFind(x *intset, i int) bool {
	return false
}
func intsetRandom(x *intset) int {
	return 0 // todo
}
func intsetGet(x *intset, i int) int {
	return 0 // todo
}
func intsetLen(x *intset) int {
	return 0 //todo
}
func intsetBlobLen(x *intset) int {
	return 0 //todo
}

///////////////////////////////////////////////////
type dictht struct {
	table                **dictEntry
	used, size, sizemask uint64
}
type dictEntry struct {
	key  *interface{}
	v    *interface{}
	next *dictEntry
}
type dict struct {
	_type      *dictType
	privdata   *interface{}
	ht         [2]dictht
	trehashidx int
}
type dictType struct {
	hashFunction   func(key *interface{}) uint
	keyDup         func(privdata *interface{}, key *interface{}) *interface{}
	valDup         func(privdata *interface{}, key *interface{}) *interface{}
	keyCompare     func(privdata *interface{}, key *interface{}) int
	keyDestructor  func(privdata *interface{}, key *interface{}) int
	vailDestructor func(privdata *interface{}, key *interface{}) int
}

func dictCreate() *dict {
	return nil //todo
}
func dictAdd(x *dict, k *interface{}, v *interface{}) {

}

func dictReplace(x *dict, k *interface{}, v *interface{}) {

}

func dictFetchValue(x *dict, k *interface{}) *interface{} {
	return nil
}

func dictGetRandomKey(x *dict, k *interface{}) *interface{} {
	return nil
}
func dictDelete(x *dict, k *interface{}) {

}
func dictRelease(x *dict) {
	//todo
}
