package redis

type redisDb struct {
	dict *dict
}
type redisClient struct {
	db *redisDb
}
type redisServer struct {
	db    *redisDb
	dbnum int
}

func save() {
	rdbSave()
}
func rdbSave() {
	rdbSave()
}

var (
	openAOF = false
	openRDB = false
)

func Load() {
	if openAOF {
		aofLoad()
	} else if openRDB {
		rdbLoad()
	}
}

var (
	time  = 90
	count = 9000
)

func main() {
	for {
		select {
			save()
		}
	}
}
